/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.services.impl;

import com.kmbic.admin.representations.*;

import java.net.MalformedURLException;


import org.apache.http.HttpResponse;
import org.codehaus.jettison.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class WebsocketListenerServiceTest {

    @InjectMocks
    WebsocketListenerServiceWithDelayQueue websocketListenerService;
    private ServerSettings serverSettings = mock(ServerSettings.class);
    private HttpResponse mockHttpResponse = mock(HttpResponse.class);
    private CameraInfo cameraInfo = mock(CameraInfo.class);
    private RegisteredAccount registeredAccount = mock(RegisteredAccount.class);


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void blockingQueueAddItemTest() throws InterruptedException, MalformedURLException, JSONException {

//        String response = websocketListenerService.doorRelease(serverSettings);

        String response = mockHttpResponse.toString();

        websocketListenerService.addItemInBlockingQueue(response, "LOCK", 1000);
        websocketListenerService.addItemInBlockingQueue(response, "LOCK", 1500);
        Assert.assertEquals(websocketListenerService.getPostponedWorkItem().size(), 1);
        Thread.sleep(2000);
        websocketListenerService.processItemFromBlockingQueue();
        Assert.assertEquals(websocketListenerService.getPostponedWorkItem().size(), 0);
    }

    @Test
    public void blockingQueueAddItemTest2() throws InterruptedException, MalformedURLException, JSONException {
//        String response = websocketListenerService.doorRelease(serverSettings);
        String response = "200 OK";

        websocketListenerService.addItemInBlockingQueue(response, "LOCK", 1000);
        websocketListenerService.addItemInBlockingQueue(response, "LOCK", 1500);
        Assert.assertEquals(websocketListenerService.getPostponedWorkItem().size(), 1);
        Thread.sleep(2000);
        websocketListenerService.takeItemFromBlockingQueue();
        Assert.assertEquals(websocketListenerService.getPostponedWorkItem().size(), 0);
    }


    @Test
    public void blockingQueueTakeItemTest() throws InterruptedException {
        Thread.sleep(1000);
        websocketListenerService.processItemFromBlockingQueue();
        Assert.assertEquals(websocketListenerService.getPostponedWorkItem().size(), 0);
    }



}
