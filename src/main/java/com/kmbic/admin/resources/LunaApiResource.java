/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.resources;

import com.kmbic.admin.representations.DailyStats;
import com.kmbic.admin.representations.EventResponseList;
import com.kmbic.admin.representations.WeeklyStats;
import com.kmbic.admin.representations.RegisteredAccount;
import com.kmbic.admin.services.impl.ReportManagementService;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 *
 */
@Path("/events")
public class LunaApiResource {

    private static Logger logger = LogManager.getLogger();

    @Autowired
    ReportManagementService reportService;

    @GET
    @Path("/getNewEvents")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLunaEvents(@Context HttpServletRequest request, @Context HttpServletResponse response)
            throws IOException, Exception {

        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");
        // logger.debug("Received a request to update the Events Table");
        EventResponseList eventList = reportService.getLunaEvents(accountLogin);
        return Response.status(Response.Status.OK).entity(eventList).build();
    }

    @GET
    @Path("/getDailyStats")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLunaDailyStats(@Context HttpServletRequest request, @Context HttpServletResponse response)
            throws IOException, Exception {

        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");
        // logger.debug("Received a request to update the Events Table");
        DailyStats dStats = reportService.getLunaDailyStats(accountLogin);
        return Response.status(Response.Status.OK).entity(dStats).build();
    }

    @GET
    @Path("/getWeeklyStats")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLunaMonthlyStats(@Context HttpServletRequest request, @Context HttpServletResponse response)
            throws IOException, Exception {

        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");
        // logger.debug("Received a request to update the Events Table");
        WeeklyStats wStats = reportService.getLunaWeeklyStats(accountLogin);
        return Response.status(Response.Status.OK).entity(wStats).build();
    }

}
