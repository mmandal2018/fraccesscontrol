package com.kmbic.admin.resources;

import com.kmbic.admin.exceptions.AdminException;
import com.kmbic.admin.representations.CameraInfo;
import com.kmbic.admin.representations.CameraList;
import com.kmbic.admin.representations.RegisteredAccount;
import com.kmbic.admin.representations.ServerSettings;
import com.kmbic.admin.representations.UserInfo;
import com.kmbic.admin.representations.UserTable;
import com.kmbic.admin.representations.XResponse;
import com.kmbic.admin.services.impl.SettingsManagementService;
import com.kmbic.admin.services.impl.WebsocketListenerService;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 *
 */
@Path("/api")
public class SettingsResource {

    static Logger logger = Logger.getLogger(SettingsResource.class.getName());

    //WebsocketListner wsListner = new WebsocketListner();
    @Autowired
    private SettingsManagementService settingsService;

    @Autowired
    private WebsocketListenerService listenerService;

    // @PostConstruct
    // public void init() {
    //     listenerService.InitEventsListener();
    // }
    @POST
    @Path("/saveServerSettings")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response saveServerSettings(@Context HttpServletRequest request,
            @Context HttpServletResponse response, @RequestBody ServerSettings settings) throws AdminException {
        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");

        boolean saveresult = settingsService.saveServerSettings(settings, accountLogin);
        if (saveresult) {
            return Response.status(Response.Status.OK).entity("Server settings information was successfully updated.").build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error saving server settings data").build();
        }
    }

    @GET
    @Path("/getServerSettings")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getServiceSettings(@Context HttpServletRequest request,
            @Context HttpServletResponse response) throws AdminException {

        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount myAccount = new RegisteredAccount();
        myAccount.setEmail("admin@test.com");

        ServerSettings settings = settingsService.getServerSettings(myAccount);
        if (settings != null) {
            return Response.status(Response.Status.OK).entity(settings).build();
        } else {
            settings = new ServerSettings();
            settings.setSettingsErrorMsg("Error getting the server settings data");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(settings).build();
        }
    }

    @POST
    @Path("/saveCameraInfo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response saveCameraInfo(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody CameraInfo caminfo) throws AdminException, IOException, JSONException, KeyManagementException {

        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount myAccount = settingsService.getAccountInfo("admin@test.com");
        if (myAccount == null) {
            // Create the default account. 
            //Beyond PoC, the account creation has to be done first and admin can only add a new camera after Logging in
            myAccount = new RegisteredAccount();
            myAccount.setEmail("admin@test.com");
            myAccount.setName("Admin");
            myAccount.setCreationTime(new Date());
            myAccount = settingsService.saveAccountInfo(myAccount);
        }

        //Create a new token corresponding to Camera on Luna
        String lunaresponse = settingsService.createLunaToken(myAccount, caminfo.getVidcamid());
        if (!lunaresponse.equals("API_ERROR")) {
            JSONObject obj = new JSONObject(lunaresponse);
            String token = obj.getString("token");
            caminfo.setToken(token);

            // First Create the fsconfig.fs file for the specific camera
            ServerSettings settings = settingsService.getServerSettings(myAccount);
            String fsClientPath = settings.getDeffsclientdir();
            boolean writeResult = settingsService.createFsConfigFile(fsClientPath, caminfo);

            boolean saveresult = settingsService.saveCameraInfo(caminfo, myAccount);
            if (saveresult) {
                return Response.status(Response.Status.OK).entity("New camera settings successfully saved.").build();
            } else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error adding new camera").build();
            }
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error adding new camera").build();
        }

    }

    @POST
    @Path("/updateCameraInfo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateCameraInfo(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody CameraInfo caminfo) throws AdminException {

        //This will have to be replaced by the actual login account from Spring Security 
        String emailAddress = "admin@test.com";
        boolean updateResult = settingsService.updateCamInfo(emailAddress, caminfo);
        if (updateResult) {
            return Response.status(Response.Status.OK).entity("Camera settings successfully updated.").build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error adding new camera").build();
        }
    }

    @POST
    @Path("/startFSClient")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateCameraFRstatus(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody CameraInfo caminfo) throws AdminException, IOException, InterruptedException {

        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");
        ServerSettings serverSettings = settingsService.getServerSettings(accountLogin);
        CameraInfo camInfo = settingsService.getCameraInfobyID(accountLogin.getEmail(), caminfo.getCamId());

        if ((camInfo != null) && (serverSettings != null)) {
            camInfo.setFrstatus(caminfo.getFrstatus());
            long pid = settingsService.startFSClient(serverSettings, camInfo);

            if (pid != -1) { // FS Client started Successfully
                //Update the CamInfo with the PID value
                camInfo.setClientpid(Long.toString(pid));

                boolean updateResult = settingsService.updateCameraFRandPIDStatus(accountLogin.getEmail(), camInfo);

                if (updateResult) {
                    // Started the FR service.
                    //Initialize the websocket listener for the camera as well
                    listenerService.InitEventsListener(camInfo);

                    return Response.status(Response.Status.OK).entity("Succesfully started Face Recognition for Camera " + caminfo.getCamId()).build();
                } else {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error Updating the Camera FR and PID Status " + caminfo.getCamId()).build();
                } // End UpdateResult

            } else { // Pid is -1
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error Starting Face Recognition for Camera " + caminfo.getCamId()).build();
            }

        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("No Server Settings or Client Settings found for "
                    + accountLogin.getEmail() + " " + caminfo.getCamId()).build();
        }
    }

    @POST
    @Path("/stopFSClient")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response stopFSClient(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody CameraInfo caminfo) throws AdminException, IOException, InterruptedException {

        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");
        CameraInfo camInfo = settingsService.getCameraInfobyID(accountLogin.getEmail(), caminfo.getCamId());
        boolean result = false;

        if ((camInfo != null) && camInfo.getClientpid() != null) {
            camInfo.setFrstatus("Stopped");

            long pid = Long.parseLong(camInfo.getClientpid());

            if (pid != -1) {
                result = settingsService.stopFSClient(camInfo);

                if (result) {
                    listenerService.closeWSConnection(camInfo);
                    camInfo.setClientpid("-1");
                    boolean updateResult = settingsService.updateCameraFRandPIDStatus(accountLogin.getEmail(), camInfo);

                    if (updateResult) {
                        return Response.status(Response.Status.OK).entity("Succesfully stopped Face Recognition for Camera " + caminfo.getCamId()).build();
                    } else {
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error Updating the Camera FR and PID Status " + caminfo.getCamId()).build();
                    } // End UpdateResult

                } else { // Could not stop the FS Client
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error Stopping Face Recognition Client for Camera " + caminfo.getCamId()).build();
                }
            } else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("No Valid Process ID found for "
                        + accountLogin.getEmail() + " " + caminfo.getCamId()).build();
            }

        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("No Camera Settings found for "
                    + accountLogin.getEmail() + " " + caminfo.getCamId()).build();
        }
    }

    @POST
    @Path("/delCamera")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response delCamera(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody CameraInfo caminfo) throws AdminException, KeyManagementException, MalformedURLException, JSONException {

        //This will have to be replaced by the actual login account from Spring Security 
        String emailAddress = "admin@test.com";
        RegisteredAccount myAccount = settingsService.getAccountInfo("admin@test.com");
        CameraInfo camInfo = settingsService.getCameraInfobyID(emailAddress, caminfo.getCamId());
        
        if (camInfo == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error deleting the camera").build();
        }

        // First remove the associated Token on Luna Service
        String lunaresponse = settingsService.delLunaToken(myAccount, camInfo);
        if (!lunaresponse.equals("API_ERROR")) {

            boolean delResult = settingsService.deleteCamera(emailAddress, caminfo);
            if (delResult) {
                return Response.status(Response.Status.OK).entity("Camera was successfully deleted.").build();
            } else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error deleting the camera").build();
            }
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Error deleting the camera").build();
        }
    }

    @GET
    @Path("/getCameraInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCameraInfo(@Context HttpServletRequest request,
            @Context HttpServletResponse response) throws AdminException {

        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount accountLogin = new RegisteredAccount();
        accountLogin.setEmail("admin@test.com");

        CameraList camList = settingsService.getCameraInfo(accountLogin);
        if (camList != null) {
            return Response.status(Response.Status.OK).entity(camList).build();
        } else {
            camList = new CameraList();
            camList.setSettingsErrorMsg("Error getting the camera settings data");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(camList).build();
        }

    }

    @POST
    @Path("/addUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody UserInfo userInfo) throws AdminException, UnsupportedEncodingException, IOException, MalformedURLException, JSONException, KeyManagementException {
        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount myAccount = new RegisteredAccount();
        myAccount.setEmail("admin@test.com");
        XResponse xresponse = settingsService.addNewUser(userInfo, myAccount);

        return Response.status(Response.Status.OK).entity(xresponse).build();

    }

    @GET
    @Path("/getUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@Context HttpServletRequest request,
            @Context HttpServletResponse response) throws AdminException {

        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount myAccount = new RegisteredAccount();
        myAccount.setEmail("admin@test.com");

        List<UserTable> userInfoList = settingsService.getAllUsers(myAccount);
        if (userInfoList != null) {
            return Response.status(Response.Status.OK).entity(userInfoList).build();
        } else {
            userInfoList = new ArrayList();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(userInfoList).build();
        }

    }

    @POST
    @Path("/delUser")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public Response delUser(@Context HttpServletRequest request,
            @Context HttpServletResponse response,
            @RequestBody String personId) throws AdminException, UnsupportedEncodingException, IOException, MalformedURLException, JSONException, KeyManagementException {
        //This will have to be replaced by the actual login account from Spring Security 
        RegisteredAccount myAccount = new RegisteredAccount();
        myAccount.setEmail("admin@test.com");
        XResponse xresponse = settingsService.delUser(personId, myAccount);

        return Response.status(Response.Status.OK).entity(xresponse).build();

    }

}
