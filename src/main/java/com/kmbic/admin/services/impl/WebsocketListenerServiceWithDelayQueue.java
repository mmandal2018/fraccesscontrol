package com.kmbic.admin.services.impl;


import com.kmbic.admin.repository.dao.impl.AccountInfoDao;
import com.kmbic.admin.repository.dao.impl.ServerSettingsDao;
import com.kmbic.admin.representations.CameraInfo;
import com.kmbic.admin.representations.EventObject;
import com.kmbic.admin.representations.RegisteredAccount;
import com.kmbic.admin.representations.ServerSettings;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;

    /**
     *
     * @author Kviswanath
     */
    public class WebsocketListenerServiceWithDelayQueue {

        @Autowired
        private ServerSettingsDao serverSettingsDao;

        @Autowired
        private AccountInfoDao accountsDao;

        static Logger log = Logger.getLogger(com.kmbic.admin.services.impl.WebsocketListenerServiceWithDelayQueue.class.getName());
        HashMap<String, WebSocket> wsconnections = new HashMap<>();

        private final static long delay = 1000;
        private final BlockingQueue<EventObject> postponedWorkItem = new DelayQueue<EventObject>();

        public WebsocketListenerServiceWithDelayQueue() {

        }

        public BlockingQueue<EventObject> getPostponedWorkItem() {
            return postponedWorkItem;
        }

        public void InitEventsListener(final CameraInfo camInfo) {

            try {
                RegisteredAccount myAccount = new RegisteredAccount();
                myAccount.setEmail("admin@test.com");
                final ServerSettings serverSettings = serverSettingsDao.getServerSettings(myAccount);
                if (serverSettings != null) {
                    String serverIP = serverSettings.getServerip();
                    //String fsToken = serverSettings.getFsToken();
                    String fsToken = camInfo.getToken();
                    String wsEndpoint = "ws://" + serverIP + ":5008/api/subscribe?auth_token=" + fsToken + "&event_type=match";

                    WebSocket ws = new WebSocketFactory()
                            .createSocket(wsEndpoint)
                            .addListener(new WebSocketAdapter() {
                                @Override
                                public void onTextMessage(WebSocket ws, String eventstream) throws MalformedURLException, JSONException, InterruptedException {

                                    //log.error(eventstream);
                                    parseEventsandSendSignal(eventstream, camInfo);
                                    String moxaResponse = doorRelease(serverSettings);

                                    if (!moxaResponse.equals("API_ERROR")) {
                                        try {
                                            addItemInBlockingQueue(moxaResponse, "LOCK", 1000);
                                            lockDoor(serverSettings);
                                            Thread.sleep(2000);
                                            processItemFromBlockingQueue();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            })
                            .connect()
                            .sendText("Hello.");

                    if (ws != null) {
                        wsconnections.put(camInfo.getVidcamid(), ws);
                    }

                }
            } catch (IOException | WebSocketException ex) {
                log.error("Error " + ex);
            }

        }

        private boolean parseEventsandSendSignal(String events, CameraInfo camInfo) throws MalformedURLException, JSONException {
            boolean result = false;
            Double similarity = 0.0;
            String url;
            JSONObject jObject = new JSONObject(events);
            JSONObject obj1 = jObject.getJSONObject("result");
            JSONArray candidates = obj1.getJSONArray("candidates");
            if (candidates.length() > 0) {
                JSONObject obj2 = (JSONObject) candidates.get(0);
                String score = obj2.getString("similarity");
                if (!score.equals("null")) {
                    similarity = Double.parseDouble(score) * 100;
                }
            }

            // log.error("Match Similarity is " + similarity);
            //This should be obtained from the events
            //String cameraId = "Mobotix ATL";
            //String cameraId = camInfo.getVidcamid();
            RegisteredAccount myAccount = new RegisteredAccount();
            myAccount.setEmail("admin@test.com");

            //CameraInfo camInfo = accountsDao.getCameraInfobyName(myAccount.getEmail(), cameraId);
            if (camInfo != null) {

                String camIP = camInfo.getVidcamip();
                if (similarity > 75) {
                    url = "http://" + camIP + "/control/rcontrol?action=ledsstring&leds=001000&time=5";
                } else {
                    url = "http://" + camIP + "/control/rcontrol?action=ledsstring&leds=000100&time=5";
                }
                URL object = new URL(url);
                String response = "";
                try {
                    HttpURLConnection con = (HttpURLConnection) object.openConnection();

                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.setRequestMethod("GET");

                    StringBuilder sb = new StringBuilder();
                    int HttpResult = con.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(con.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();

                    } else {
                        result = false;
                        log.error("Error sending LED signals to Mobotix Camera " + camInfo.getVidcamid());
                    }

                } catch (IOException e) {
                    result = false;
                    log.error("Cannot connect to Mobotix Camera " + camInfo.getVidcamid());

                }

            }
            return result;
        }

        public String doorRelease(ServerSettings serverSettings) throws MalformedURLException, JSONException {

            String moxagetResponse = "";
            String moxaResponse = "";

            if (serverSettings != null) {
                String moxaIP = serverSettings.getMoxaip();
                String openTime = serverSettings.getDoorreleasetime();

                //String moxaUrl = "http://" + moxaIP + "/api/slot/0/io/relay/2/relayStatus";
                String moxaUrl = "http://10.1.255.206/api/slot/0/io/relay/2/relayStatus";
                URL object = new URL(moxaUrl);

                //First do a get befor eyou can PUT
                try {
                    HttpURLConnection con = (HttpURLConnection) object.openConnection();

                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "vdn.dac.v1");
                    con.setRequestMethod("PUT");

                    //JSONObject request = new JSONObject();
                    //JSONObject request = new JSONObject("{\"slot\":0,\"io\":{\"relay\":{\"2\":{\"relayStatus\":1}}}}");
                    String request = "{\"slot\":0,\"io\":{\"relay\":{\"2\":{\"relayStatus\":1}}}}";
                    OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                    wr.write(request);
                    wr.flush();
                    wr.close();

                    //display what returns the POST request
                    StringBuilder sb = new StringBuilder();
                    int HttpResult = con.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(con.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();
                        //System.out.println("" + sb.toString());
                        moxaResponse = sb.toString();

                    } else {
                        moxaResponse = "API_ERROR";
                    }

                } catch (IOException e) {
                    log.error("Cannot connect to MOXA API Server");
                    return "API_ERROR";
                }
            } else {
                log.error("Cannot find Moxa IP settings");
                return "API_ERROR";
            }
            return moxaResponse;

        }

        private void lockDoor(final ServerSettings serverSettings) {

//        int holdtime = Integer.parseInt(serverSettings.getDoorreleasetime());
//        int timer = holdtime * 1000;

//        new java.util.Timer().schedule(
//                new java.util.TimerTask() {
//                    @Override
//                    public void run() {
            String moxaResponse = "";

            if (serverSettings != null) {
                String moxaIP = serverSettings.getMoxaip();

                //String moxaUrl = "http://" + moxaIP + "/api/slot/0/io/relay/2/relayStatus";
                String moxaUrl = "http://10.1.255.206/api/slot/0/io/relay/2/relayStatus";
                URL url = null;
                try {
                    url = new URL(moxaUrl);
                } catch (MalformedURLException ex) {
                    log.error("Error in Moxa URL");
                }

                //First do a get befor eyou can PUT
                try {
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();

                    con.setDoOutput(true);
                    con.setDoInput(true);
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "vdn.dac.v1");
                    con.setRequestMethod("PUT");

                    //JSONObject request = new JSONObject();
                    //JSONObject request = new JSONObject("{\"slot\":0,\"io\":{\"relay\":{\"2\":{\"relayStatus\":1}}}}");
                    String request = "{\"slot\":0,\"io\":{\"relay\":{\"2\":{\"relayStatus\":0}}}}";
                    OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                    wr.write(request);
                    wr.flush();
                    wr.close();

                    //display what returns the POST request
                    StringBuilder sb = new StringBuilder();
                    int HttpResult = con.getResponseCode();
                    if (HttpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(
                                new InputStreamReader(con.getInputStream(), "utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();
                        //System.out.println("" + sb.toString());
                        moxaResponse = sb.toString();

                    } else {
                        moxaResponse = "API_ERROR";
                    }

                } catch (IOException e) {
                    log.error("Cannot connect to MOXA API Server");
                }
            } else {
                log.error("Cannot find Moxa IP settings");

            }

//                    }

//                }, timer);

        }

        public void closeWSConnection(CameraInfo camInfo) {
            WebSocket ws = wsconnections.get(camInfo.getVidcamid());
            if (ws != null) {
                log.info("Closing ws connection corresponding to " + camInfo.getVidcamid());
                ws.disconnect();
            }

        }

        public void addItemInBlockingQueue(String moxaResponse, String status, int delay) throws InterruptedException {
            EventObject postponed = new EventObject(moxaResponse, status, delay );

            if(postponedWorkItem.size() > 0){
                processItemFromBlockingQueue();
            }
            if( !postponedWorkItem.contains( postponed) && postponedWorkItem.size() == 0) {
                System.out.println("PUT" + postponed);
                postponedWorkItem.offer(postponed, 5000, TimeUnit.MILLISECONDS);
            }
        }

        public  void processItemFromBlockingQueue() throws InterruptedException {
            final List<EventObject> expired = new ArrayList<EventObject>();
            postponedWorkItem.drainTo(expired);
            System.out.println("Current Time: drain" + new Date());

            if(expired.size() > 0) {
                for (final EventObject postponed : expired) {
                    System.out.println("Current Time: drain" + new Date());
                    System.out.println("TAKE" + postponed);
                }
            }
            else {
                System.out.println("not Expired:  " + postponedWorkItem.poll());
            }
        }

        public  void takeItemFromBlockingQueue() throws InterruptedException {
            while (!postponedWorkItem.isEmpty()) {
                try {
                    System.out.println("Current Time: take" + new Date());
                    EventObject object = postponedWorkItem.take();
                    System.out.printf("[%s] - Take object = %s%n",
                            Thread.currentThread().getName(), object);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

