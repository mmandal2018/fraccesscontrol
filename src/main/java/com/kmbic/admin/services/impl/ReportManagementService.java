package com.kmbic.admin.services.impl;

import com.kmbic.admin.repository.dao.impl.ServerSettingsDao;
import com.kmbic.admin.representations.DailyStats;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.kmbic.admin.representations.EventResponse;
import com.kmbic.admin.representations.EventResponseList;
import com.kmbic.admin.representations.WeeklyStats;
import com.kmbic.admin.representations.RegisteredAccount;
import com.kmbic.admin.representations.ServerSettings;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import javax.imageio.ImageIO;
import org.apache.logging.log4j.LogManager;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import com.kmbic.admin.utils.HttpsTrustManager;

public class ReportManagementService {

    @Autowired
    private ServerSettingsDao serverSettingsDao;

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();

    private static final int MAX_EVENTS = 20;
    private static final String LUNA_EVENT_API = "/facestream/view/all";

    private static final String unknown_user = "iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAQAAACSR7JhAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAA9KMAAPSjAdI7XwgAAAAHdElNRQfhCw4RDxQGZSVQAAAE3UlEQVRo3uWaXWhcRRSAv7v/P81fm2xS200lCRHBhhYfNNpq8iK1loJgsYH6j5Uo+KQVFJGKiBZLY+tDQURBDD5EhdaKvrRWK5LWaGOpFEHzY2pkS3abpNlNNsleH3Zdtrtz787s3r0GPfOSO3fOnO+emTNzZrIa5UmQDrZyB+00EcRJgquM8BOnGeRP9DJ7L0kaeIwTRFhGLyhxhnmDTbjsRQqyh7MsCoByyyR9tNkH1U4/iSJI/5Rf2I3bDqguzksipcsMr7Kq0lD3MaIEpaOT5AjVlfWUOpSOziIH8FUKqo0hQ8PzRLlCjAWD9wl65Q1pClB+jvKwoH6BQU4wzBWSeGlkMzu4VTDNJ3iAQet99SBzAi/8zuPU5LVcw1OMCdoOELQaajXfCKHuNmi/jcuCRfZ+q7F6BLMmziMmGs8J1v9jBKyE8jAg8NWX161Hjrz5tIFfCzSi3C5jziGJ1UqnoPYLruVAvMMn9OQE0QTnCzTquEfGnOxWupnGgrpZhnKeHqUXWM9XRDM1y0QEPXXiY946LGdBXZIWXBl/a2wF4ALx7HsHdYKebiLEuKTVovCfCmZWigTxTEmwyCVeJJSjtY6LAq1pbrMGClZxpujmcpQbr1ucXbwizMSS7LAKq44fi2Adz1tSG9nPjLDlMrtlhkdGtCIRu8SHTGefqthFr3A2pkUi+uWwFovETpSL2b+r6WOPSeKXygkKQ5FbtxayQS+WZI6pziLZaLG+FLyVZNT0fS0vEUMDUtyC37TtVSatwoKfTd/6eSgnCnXTdGmUv6zDGiImXBzTcprXSJ8JU4Tpo96kp3PMStqUkCpOmSwP/TlztI0Jk5ZxtsuYk92qZzlmyecNy+WnsljwGZfKhkrxEVPWYo3yPqkysc4xINdQHgs+4FuDNy4CmeLDbxiH1zgoE4WgdvKBLvpZK6iPZAdYJ0AHXkEbncPsI6lkT1r2MlvS8VXnc0EiaZm4eUF4LCtWTtFaOSgAD88ypYSU4jgtlYUCcLKTC9JQM7xFQ+Wh0tLKEQmfLXKGnXjsggJwcyfv8ocwMdbRmeVrnjDdHU1EbYHIFxctdLOFjTRShQuNZeaJ8htnOckQM6V2XB5WWpzU0kB95qZ5mghTJCzod+WJswxdDQ+ayT7pxlnqLqo2iE58VLOGJtYRpplmvudNg+NHDftpYYRRxpggQow5lmQNmWenGh6C1BJiLWGaCXMDIeqowpv5oLuY5rDAJ26e55lM70vEiRHhMmOMMJ6BjJtBGnkrxDY20Mx6mqinmoDhB8R4mo8Lap/kkMHN3xJzxIgwwRhjfMcPKsO13fBqtrCM052nfS+Tkrrvic0b5VsBhWAI8zYdOc+bOEiTpK5PTGCMpZIgbqSP5izkIW6W1vSKp5GRcb9ijHZzgNVADa/TpaDnUfWWquziZWrYR4+SlgGWUXyZH9jFH7iXdrYoLtCKWKVcUwfkjqZ5WMLPsG4QS5OKz63SxK3iLa2EuVUqlnAaibEcNnpLCWtFestZuf+U5olLBcv1f8dyiq9/jbC82CMuFSyPbVhK3nLbdhJ2ii0ZecsuLIcKlteeX8Wg6C37sJS85bPtd1dKWP6yTtsqoolj3shbdmE5VLDs8xZqg2jFBZOcKHgrsDKx7Mq2FLHsyk3/G1j/+iD+DV+txAQPliduAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTExLTE0VDE3OjE1OjIwLTA1OjAwmsu6rQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNy0xMS0xNFQxNzoxNToyMC0wNTowMOuWAhEAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC";

    public EventResponseList getLunaEvents(RegisteredAccount accountLogin) throws IOException, Exception {

        EventResponseList eventList = new EventResponseList();
        List<EventResponse> xresponseList = new ArrayList<>();
        int max_events = 0;
        Double score = 0.0;

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(accountLogin);

        if (serverSettings == null) {
            logger.error("No server Settings found for Account " + accountLogin.getEmail());
            EventResponse xresponse = new EventResponse();
            xresponse.setErrorCode(1);
            xresponseList.add(xresponse);
            eventList.setXresponselist(xresponseList);
            return eventList;
        }

        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport();

        String ApiResponse = getEventsfromLuna(accountLogin, baseUrl);

        if (ApiResponse.equalsIgnoreCase("API_ERROR")) {
            EventResponse xresponse = new EventResponse();
            xresponse.setErrorCode(1);
            xresponseList.add(xresponse);

        } else {
            try {
                //JSONObject apiresponse = new JSONObject(ApiResponse);
                JSONArray events = new JSONArray(ApiResponse);

                if (events.length() > MAX_EVENTS) {
                    max_events = MAX_EVENTS;
                } else {
                    max_events = events.length();
                }

                for (int i = 0; i < max_events; i++) {
                    EventResponse xresponse = new EventResponse();
                    xresponse.setEventNum(i + 1);
                    JSONObject obj1 = (JSONObject) events.get(i);

                    String descriptorId = obj1.getString("descriptor_id");
                    int eventId = obj1.getInt("event_id");
                    String search_result = obj1.getString("search_result");
                    String personId = obj1.getString("person_id");
                    String userData = obj1.getString("user_data");
                    String scoreStr = obj1.getString("similarity");
                    if (!scoreStr.equals("null")) {
                        score = Double.parseDouble(scoreStr);
                    } else {
                        score = 0.0;
                    }
                    xresponse.setMatchPercentage(score);

                    String eventTime = obj1.getString("event_time");
                    xresponse.setEventTime(eventTime);

                    String cameraId = obj1.getString("source_id");
                    xresponse.setCamId(cameraId);

                    String camImage = getCamImageforEventId(baseUrl, eventId);
                    xresponse.setImageStr(camImage);

                    if (score > 60) { // Set the Score threshold as 60%
                        String enrolledUserImage = getUserImageforEventId(baseUrl, eventId);
                        xresponse.setEnrolledImageStr(enrolledUserImage);
                        xresponse.setName(userData);
                        xresponse.setAuthorized("Yes");
                    } else {
                        xresponse.setEnrolledImageStr("");
                        xresponse.setName("");
                        xresponse.setAuthorized("No");
                    }

                    xresponse.setErrorCode(0);
                    xresponseList.add(xresponse);
                }

            } catch (JSONException e) {
                logger.error("Error Parsing JSON Response from Luna Server");
                EventResponse xresponse = new EventResponse();
                xresponse.setErrorCode(1);
                xresponseList.add(xresponse);
            }

        }

        eventList.setXresponselist(xresponseList);
        return eventList;
    }

    public String getEventsfromLuna(RegisteredAccount accountLogin, String baseUrl) throws MalformedURLException, KeyManagementException {
        HttpsTrustManager.allowAllSSL();
        String url = baseUrl + LUNA_EVENT_API;
        URL object = new URL(url);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();

            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            //JSONObject request = new JSONObject();
            //request.put("maxCount", 10);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            //wr.write(request.toString());
            wr.flush();

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {

                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            logger.error("Cannot connect to LUNA API Server");
            return "API_ERROR";
        }

        return lunaResponse;
    }

    private String getCamImageforEventId(String baseUrl, int eventId) throws MalformedURLException, IOException, KeyManagementException {
        String lunaResponse = "";
        HttpsTrustManager.allowAllSSL();
        String api = baseUrl + "/client/facestream/event/" + eventId + "/eventPhoto";
        URL url = new URL(api);

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setDoOutput(true);
        con.setDoInput(true);
        String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
        con.setRequestProperty("Authorization", basicAuth);
        // con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "image/jpeg");
        con.setRequestMethod("GET");
        int HttpResult = con.getResponseCode();
        if (HttpResult == HttpURLConnection.HTTP_OK) {
            BufferedInputStream in = new BufferedInputStream(con.getInputStream());
            ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            int c;
            while ((c = in.read()) != -1) {
                byteArrayOut.write(c);
            }

            InputStream input = new ByteArrayInputStream(byteArrayOut.toByteArray());
            BufferedImage profileImg = ImageIO.read(input);
            BufferedImage thumbnail = Scalr.resize(profileImg, Scalr.Method.SPEED, 100);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(thumbnail, "jpeg", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            lunaResponse = java.util.Base64.getEncoder().encodeToString(imageInByte);

        } else {
            lunaResponse = "API_ERROR";
        }

        return lunaResponse;

    }

    private String getUserImageforEventId(String baseUrl, int eventId) throws MalformedURLException, IOException, KeyManagementException {
        String lunaResponse = "";
        HttpsTrustManager.allowAllSSL();
        String api = baseUrl + "/client/facestream/event/" + eventId + "/simPhoto";
        URL url = new URL(api);

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setDoOutput(true);
        con.setDoInput(true);
        String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
        con.setRequestProperty("Authorization", basicAuth);
        // con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "image/jpeg");
        con.setRequestMethod("GET");
        int HttpResult = con.getResponseCode();
        if (HttpResult == HttpURLConnection.HTTP_OK) {
            BufferedInputStream in = new BufferedInputStream(con.getInputStream());
            ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            int c;
            while ((c = in.read()) != -1) {
                byteArrayOut.write(c);
            }

            InputStream input = new ByteArrayInputStream(byteArrayOut.toByteArray());
            BufferedImage profileImg = ImageIO.read(input);
            BufferedImage thumbnail = Scalr.resize(profileImg, Scalr.Method.SPEED, 100);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(thumbnail, "jpeg", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            lunaResponse = java.util.Base64.getEncoder().encodeToString(imageInByte);

        } else {
            lunaResponse = "API_ERROR";
        }

        return lunaResponse;
    }

    public DailyStats getLunaDailyStats(RegisteredAccount accountLogin) throws KeyManagementException, MalformedURLException, JSONException {

        DailyStats dailyStats = new DailyStats();
        int numIdentified = 0;
        int numUnIdentified = 0;

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(accountLogin);

        if (serverSettings == null) {
            logger.error("No server Settings found for Account " + accountLogin.getEmail());
            return null;
        }
        String baseUrl = "http://" + serverSettings.getServerip() + ":5008";

        String ApiResponse = getDailyStats(baseUrl, "Identified");

        if (!ApiResponse.equalsIgnoreCase("API_ERROR")) {
            numIdentified = parseLunaEventResponse(ApiResponse);
        }
        String ApiResponse1 = getDailyStats(baseUrl, "UnIdentified");

        if (!ApiResponse1.equalsIgnoreCase("API_ERROR")) {
            numUnIdentified = parseLunaEventResponse(ApiResponse1);
        }
        dailyStats.setIdentified(numIdentified);
        dailyStats.setUnidentified(numUnIdentified);
        return dailyStats;
    }

    private String getDailyStats(String baseUrl, String matchType) throws KeyManagementException, MalformedURLException {
        HttpsTrustManager.allowAllSSL();
        String url = null;

        switch (matchType) {

            case "Identified":
                url = baseUrl + "/api/events?event_type=match&source=search&similarity__gt=0.75&aggregator=count&time__gt=now-24h&time__lt=now&group_step=25h";
                break;

            case "UnIdentified":
                url = baseUrl + "/api/events?event_type=match&source=search&similarity__lt=0.75&aggregator=count&time__gt=now-24h&time__lt=now&group_step=25h";
                break;
        }

        URL object = new URL(url);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();

            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestMethod("GET");

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {

                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            logger.error("Cannot connect to LUNA API Server");
            return "API_ERROR";
        }

        return lunaResponse;

    }

    private int parseLunaEventResponse(String ApiResponse) throws JSONException {
        int result = 0;
        JSONObject obj1 = new JSONObject(ApiResponse);
        JSONArray values = obj1.getJSONArray("values");
        if (values.length() > 0) {
            JSONArray firstElement = (JSONArray) values.get(0);
            if (firstElement != null) {
                result = firstElement.getInt(5);
            }

        }

        return result;
    }

    public WeeklyStats getLunaWeeklyStats(RegisteredAccount accountLogin) throws KeyManagementException, MalformedURLException, MalformedURLException, JSONException {
        WeeklyStats wStats = new WeeklyStats();
        WeeklyStats matchStats = new WeeklyStats();
        WeeklyStats mismatchStats = new WeeklyStats();

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(accountLogin);

        if (serverSettings == null) {
            logger.error("No server Settings found for Account " + accountLogin.getEmail());
            return null;
        }
        String baseUrl = "http://" + serverSettings.getServerip() + ":5008";
        String ApiResponse = getWeeklyStats(baseUrl, "Identified");
        if (!ApiResponse.equalsIgnoreCase("API_ERROR")) {
            matchStats = parseLunaWeeklyEventResponse(ApiResponse, "Identified");
        }
        String ApiResponse1 = getWeeklyStats(baseUrl, "UnIdentified");

        if (!ApiResponse1.equalsIgnoreCase("API_ERROR")) {
            mismatchStats = parseLunaWeeklyEventResponse(ApiResponse1, "UnIdentified");
        }
        wStats.setMatchDate(matchStats.getMatchDate());
        wStats.setMatchVal(matchStats.getMatchVal());
        wStats.setMismatchdate(mismatchStats.getMismatchdate());
        wStats.setMismatchVal(mismatchStats.getMismatchVal());
        return wStats;
    }

    private String getWeeklyStats(String baseUrl, String matchType) throws KeyManagementException, MalformedURLException {
        HttpsTrustManager.allowAllSSL();
        String url = null;

        switch (matchType) {

            case "Identified":
                url = baseUrl + "/api/events?event_type=match&source=search&similarity__gt=0.75&aggregator=count&time__gt=now-2016h&time__lt=now&group_step=168h";
                break;

            case "UnIdentified":
                url = baseUrl + "/api/events?event_type=match&source=search&similarity__lt=0.75&aggregator=count&time__gt=now-2016h&time__lt=now&group_step=168h";
                break;
        }

        URL object = new URL(url);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();

            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestMethod("GET");

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {

                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            logger.error("Cannot connect to LUNA API Server");
            return "API_ERROR";
        }

        return lunaResponse;

    }

    private WeeklyStats parseLunaWeeklyEventResponse(String ApiResponse, String matchType) throws JSONException {
        WeeklyStats wStats = new WeeklyStats();
        ArrayList<String> dateList = new ArrayList();
        ArrayList<Integer> matchList = new ArrayList();
        JSONObject obj1 = new JSONObject(ApiResponse);
        JSONArray values = obj1.getJSONArray("values");
        if (values.length() > 0) {
            for (int i = 0; i < values.length(); i++) {
                JSONArray element = (JSONArray) values.get(i);
                dateList.add(element.getString(0));
                matchList.add(element.getInt(5));
            }

        }

        switch (matchType) {
            case "Identified":
                wStats.setMatchDate(dateList);
                wStats.setMatchVal(matchList);
                break;

            case "UnIdentified":
                wStats.setMismatchdate(dateList);
                wStats.setMismatchVal(matchList);
                break;
        }

        return wStats;
    }
}
