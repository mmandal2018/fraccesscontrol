/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.services.impl;

import com.kmbic.admin.repository.dao.impl.AccountInfoDao;
import com.kmbic.admin.repository.dao.impl.EnrolledUserDao;
import com.kmbic.admin.repository.dao.impl.ServerSettingsDao;
import com.kmbic.admin.representations.CameraInfo;
import com.kmbic.admin.representations.CameraList;
import com.kmbic.admin.representations.ReadStream;
import com.kmbic.admin.representations.RegisteredAccount;
import com.kmbic.admin.representations.ServerSettings;
import com.kmbic.admin.representations.UserInfo;
import com.kmbic.admin.representations.UserTable;
import com.kmbic.admin.representations.XResponse;
import com.kmbic.admin.utils.HttpsTrustManager;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinNT;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.imageio.ImageIO;
import static org.apache.commons.io.CopyUtils.copy;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.imgscalr.Scalr;

/**
 *
 * @author kmbic
 */
public class SettingsManagementService {

    @Autowired
    private ServerSettingsDao serverSettingsDao;

    @Autowired
    private AccountInfoDao accountInfoDao;

    @Autowired
    private EnrolledUserDao userInfoDao;

    static Logger log = Logger.getLogger(SettingsManagementService.class.getName());
    static String fsConfigFileName = "fs2Config.cfg";

    private static String unknown_user = "iVBORw0KGgoAAAANSUhEUgAAAEsAAABLCAQAAACSR7JhAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAA9KMAAPSjAdI7XwgAAAAHdElNRQfhCw4RDxQGZSVQAAAE3UlEQVRo3uWaXWhcRRSAv7v/P81fm2xS200lCRHBhhYfNNpq8iK1loJgsYH6j5Uo+KQVFJGKiBZLY+tDQURBDD5EhdaKvrRWK5LWaGOpFEHzY2pkS3abpNlNNsleH3Zdtrtz787s3r0GPfOSO3fOnO+emTNzZrIa5UmQDrZyB+00EcRJgquM8BOnGeRP9DJ7L0kaeIwTRFhGLyhxhnmDTbjsRQqyh7MsCoByyyR9tNkH1U4/iSJI/5Rf2I3bDqguzksipcsMr7Kq0lD3MaIEpaOT5AjVlfWUOpSOziIH8FUKqo0hQ8PzRLlCjAWD9wl65Q1pClB+jvKwoH6BQU4wzBWSeGlkMzu4VTDNJ3iAQet99SBzAi/8zuPU5LVcw1OMCdoOELQaajXfCKHuNmi/jcuCRfZ+q7F6BLMmziMmGs8J1v9jBKyE8jAg8NWX161Hjrz5tIFfCzSi3C5jziGJ1UqnoPYLruVAvMMn9OQE0QTnCzTquEfGnOxWupnGgrpZhnKeHqUXWM9XRDM1y0QEPXXiY946LGdBXZIWXBl/a2wF4ALx7HsHdYKebiLEuKTVovCfCmZWigTxTEmwyCVeJJSjtY6LAq1pbrMGClZxpujmcpQbr1ucXbwizMSS7LAKq44fi2Adz1tSG9nPjLDlMrtlhkdGtCIRu8SHTGefqthFr3A2pkUi+uWwFovETpSL2b+r6WOPSeKXygkKQ5FbtxayQS+WZI6pziLZaLG+FLyVZNT0fS0vEUMDUtyC37TtVSatwoKfTd/6eSgnCnXTdGmUv6zDGiImXBzTcprXSJ8JU4Tpo96kp3PMStqUkCpOmSwP/TlztI0Jk5ZxtsuYk92qZzlmyecNy+WnsljwGZfKhkrxEVPWYo3yPqkysc4xINdQHgs+4FuDNy4CmeLDbxiH1zgoE4WgdvKBLvpZK6iPZAdYJ0AHXkEbncPsI6lkT1r2MlvS8VXnc0EiaZm4eUF4LCtWTtFaOSgAD88ypYSU4jgtlYUCcLKTC9JQM7xFQ+Wh0tLKEQmfLXKGnXjsggJwcyfv8ocwMdbRmeVrnjDdHU1EbYHIFxctdLOFjTRShQuNZeaJ8htnOckQM6V2XB5WWpzU0kB95qZ5mghTJCzod+WJswxdDQ+ayT7pxlnqLqo2iE58VLOGJtYRpplmvudNg+NHDftpYYRRxpggQow5lmQNmWenGh6C1BJiLWGaCXMDIeqowpv5oLuY5rDAJ26e55lM70vEiRHhMmOMMJ6BjJtBGnkrxDY20Mx6mqinmoDhB8R4mo8Lap/kkMHN3xJzxIgwwRhjfMcPKsO13fBqtrCM052nfS+Tkrrvic0b5VsBhWAI8zYdOc+bOEiTpK5PTGCMpZIgbqSP5izkIW6W1vSKp5GRcb9ijHZzgNVADa/TpaDnUfWWquziZWrYR4+SlgGWUXyZH9jFH7iXdrYoLtCKWKVcUwfkjqZ5WMLPsG4QS5OKz63SxK3iLa2EuVUqlnAaibEcNnpLCWtFestZuf+U5olLBcv1f8dyiq9/jbC82CMuFSyPbVhK3nLbdhJ2ii0ZecsuLIcKlteeX8Wg6C37sJS85bPtd1dKWP6yTtsqoolj3shbdmE5VLDs8xZqg2jFBZOcKHgrsDKx7Mq2FLHsyk3/G1j/+iD+DV+txAQPliduAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE3LTExLTE0VDE3OjE1OjIwLTA1OjAwmsu6rQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNy0xMS0xNFQxNzoxNToyMC0wNTowMOuWAhEAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC";

    public RegisteredAccount getAccountInfo(String emailAddress) {
        RegisteredAccount account = accountInfoDao.getAccountInfobyEmail(emailAddress);
        return account;

    }

    public boolean saveServerSettings(ServerSettings settings, RegisteredAccount accountLogin) {
        boolean saveResult = serverSettingsDao.saveServerSettings(settings, accountLogin);
        return saveResult;

    }

    public ServerSettings getServerSettings(RegisteredAccount accountLogin) {
        ServerSettings settinginfo = serverSettingsDao.getServerSettings(accountLogin);
        return settinginfo;

    }

    public boolean saveCameraInfo(CameraInfo camInfo, RegisteredAccount accountLogin) {
        boolean saveResult = accountInfoDao.saveCameraInfo(camInfo, accountLogin);
        return saveResult;

    }

    public CameraList getCameraInfo(RegisteredAccount accountLogin) {
        CameraList camList = accountInfoDao.getCameraInfoList(accountLogin);
        return camList;

    }

    public RegisteredAccount saveAccountInfo(RegisteredAccount myAccount) {
        RegisteredAccount newAccount = accountInfoDao.saveAccountInfo(myAccount);
        return newAccount;
    }

    public long startFSClient(ServerSettings serverSettings, CameraInfo cameraInfo) throws IOException, InterruptedException {

        long pid = -1;
        String fsPath = serverSettings.getDeffsclientdir();
        File fsDir = new File(fsPath);
        String fsExe = "FaceStream2.exe";

        File f = new File(fsPath + File.separator + fsExe);
        if (!f.exists()) {
            log.error("FS client.exe not found");
            return -1;
        }

        String configFileDir = fsPath + File.separator + "data";
        String camName = cameraInfo.getVidcamid().replaceAll("\\s+", "");

        File configFile = new File(configFileDir + File.separator + fsConfigFileName + "." + camName);

        if (!configFile.exists()) {
            log.error("FS Config File not found");
            return -1;
        }

        String cmd = fsPath + "\\FaceStream2.exe -Src " + cameraInfo.getVidcamprotocol() + "://" + cameraInfo.getVidcamuserid() + ":" + cameraInfo.getVidcampwd()
                + "@" + cameraInfo.getVidcamip() + ":" + cameraInfo.getVidcamport() + "/" + cameraInfo.getVidcamstreampath()
                + " -Dst https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + serverSettings.getServerendpoint()
                + " -Cp " + configFileDir + File.separator + fsConfigFileName + "." + camName;

        Process p = null;

        try {
            p = Runtime.getRuntime().exec(cmd, null, fsDir);
            Thread.sleep(3000);
            p.getInputStream().close();
            p.getOutputStream().close();
            p.getErrorStream().close();

            // ReadStream s1 = new ReadStream("stdin", p.getInputStream());
            //ReadStream s2 = new ReadStream("stderr", p.getErrorStream());
            //s1.start();
            //s2.start();
            //p.waitFor();
            pid = getProcessID(p);
            log.info("Process Id is" + pid);
        } catch (IOException e) {
            log.error("Exception starting FSClient for cameraId " + cameraInfo.getVidcamid() + " " + e);
            return pid;
        }

        return pid;

    }

    public boolean updateCamInfo(String emailAddress, CameraInfo caminfo) {
        boolean saveResult = accountInfoDao.updateCameraInfo(emailAddress, caminfo);
        return saveResult;
    }

    public boolean deleteCamera(String emailAddress, CameraInfo caminfo) {
        boolean delResult = accountInfoDao.deleteCamera(emailAddress, caminfo);
        return delResult;

    }

    public boolean updateCameraFRandPIDStatus(String emailAddress, CameraInfo caminfo) {
        boolean saveResult = accountInfoDao.updateCameraFRandPIDStatus(emailAddress, caminfo);
        return saveResult;

    }

    public CameraInfo getCameraInfobyID(String email, int camId) {
        CameraInfo camInfo = accountInfoDao.getCameraInfobyID(email, camId);
        return camInfo;
    }

    public UserInfo getUserbyPersonId(String personId) {
        UserInfo user = userInfoDao.getUserbyPersonId(personId);
        return user;
    }

    private long getProcessID(Process p) {
        long pid = -1;
        try {
            //for windows
            if (p.getClass().getName().equals("java.lang.Win32Process")
                    || p.getClass().getName().equals("java.lang.ProcessImpl")) {
                Field f = p.getClass().getDeclaredField("handle");
                f.setAccessible(true);
                long handl = f.getLong(p);
                Kernel32 kernel = Kernel32.INSTANCE;
                WinNT.HANDLE hand = new WinNT.HANDLE();
                hand.setPointer(Pointer.createConstant(handl));
                pid = kernel.GetProcessId(hand);
                f.setAccessible(false);
            } //for unix based operating systems
            else if (p.getClass().getName().equals("java.lang.UNIXProcess")) {
                Field f = p.getClass().getDeclaredField("pid");
                f.setAccessible(true);
                pid = f.getLong(p);
                f.setAccessible(false);
            }
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
            pid = -1;
        }
        return pid;

    }

    public boolean updateClientPID(String email, CameraInfo camInfo) {
        boolean saveResult = accountInfoDao.updateClientPID(email, camInfo);
        return saveResult;

    }

    public boolean stopFSClient(CameraInfo camInfo) throws InterruptedException {

        String cmd = "cmd.exe /C taskkill.exe /f /pid " + camInfo.getClientpid();

        Process p = null;
        try {
            p = Runtime.getRuntime().exec(cmd);
            ReadStream s1 = new ReadStream("stdin", p.getInputStream());
            ReadStream s2 = new ReadStream("stderr", p.getErrorStream());
            s1.start();
            s2.start();
            int exitVal = p.waitFor();
            if (exitVal == 0) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            log.error("Exception stopping FSClient for cameraId " + camInfo.getVidcamid() + " " + e);
            return false;
        }

    }

    public boolean createFsConfigFile(String fsClientPath, CameraInfo caminfo) throws IOException, JSONException {

        String baseDir = fsClientPath + File.separator + "data";
        String camName = caminfo.getVidcamid().replaceAll("\\s+", "");

        File sourceFile = new File(baseDir + File.separator + fsConfigFileName);

        if (!sourceFile.exists()) {
            log.error("FS Config File not found");
            return false;
        }

        File fsConfigFile = new File(baseDir + File.separator + fsConfigFileName + "." + camName);

        /*try {
            FileUtils.copyFile(sourceFile, fsConfigFile);
        } catch (IOException ex) {
            log.error("Error creating the FSConfig file " + baseDir + File.separator + fsConfigFileName + "-" + caminfo.getVidcamid());
            return false;
        }*/
        String jsonString = FileUtils.readFileToString(sourceFile);
        JSONObject jObject = new JSONObject(jsonString);
        JSONObject obj1 = jObject.getJSONObject("sending");
        JSONObject obj2 = obj1.getJSONObject("camera-id");
        obj2.put("value", caminfo.getVidcamid().trim());
        JSONObject obj3 = obj1.getJSONObject("token");
        obj3.put("value", caminfo.getToken());

        String prettyJsonString = jObject.toString(2);
        FileUtils.writeStringToFile(fsConfigFile, prettyJsonString);

        return true;
    }

    public XResponse addNewUser(UserInfo userInfo, RegisteredAccount myAccount) throws UnsupportedEncodingException, IOException, MalformedURLException, JSONException, KeyManagementException {

        userInfo.setAccountemail(myAccount.getEmail());
        XResponse xresponse = new XResponse();

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(myAccount);
        String fslistId = serverSettings.getFsListId();

        if (serverSettings == null) {
            log.error("No server Settings found for Account " + myAccount.getEmail());
            xresponse.setErrorCode(1);
            xresponse.setErrorMsg("No server Settings found for Account " + myAccount.getEmail());
            return xresponse;
        }

        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + "/3";
        //First add the user using Luna APIs and then save the information locally as well.
        String personName = userInfo.getFirstName() + " " + userInfo.getLastName();

        String ApiResponse = createNewPersonwithDescriptor(baseUrl, personName);
        if (!ApiResponse.equals("API_ERROR")) {
            JSONObject ApiResult = new JSONObject(ApiResponse);
            String person_id = ApiResult.getString("person_id");
            String ApiResponse1 = extractImageDescriptor(baseUrl, userInfo.getPhotoList().get(0));

            if (!ApiResponse1.equals("API_ERROR")) {
                JSONObject obj = new JSONObject(ApiResponse1);
                JSONArray faces = obj.getJSONArray("faces");
                JSONObject obj1 = (JSONObject) faces.get(0);
                String id = obj1.getString("id");

                // Now Attach this descriptor id to the person_id
                String ApiResponse2 = attachDescriptorIdtoPerson(baseUrl, person_id, id);

                if (!ApiResponse2.equals("API_ERROR")) {
                    // All good so far. Now create the thumbnailStr and save locally as well
                    userInfo.setPerson_id(person_id);
                    if ((userInfo.getPhotoList() != null) && (!userInfo.getPhotoList().isEmpty())) {
                        userInfo.setThumbnail(createThumbnailString(userInfo.getPhotoList().get(0)));
                    } else {
                        userInfo.setThumbnail(unknown_user);
                    }
                    xresponse = userInfoDao.enrollUser(userInfo);

                    // Now attach the person to the default facestream list id
                    String ApiResponse3 = attachFSListIdtoPerson(baseUrl, person_id, fslistId);
                    if (ApiResponse3.equals("API_ERROR")) {
                        xresponse.setErrorCode(1);
                        xresponse.setErrorMsg("Error attaching Facestream List to User on Luna Server");
                    }

                } else {
                    xresponse.setErrorCode(1);
                    xresponse.setErrorMsg("Error attaching descriptor to User on Luna Server");
                }
            } else {
                xresponse.setErrorCode(1);
                xresponse.setErrorMsg("Error extracting image descriptor for User " + personName);
            }
        } else {
            xresponse.setErrorCode(1);
            xresponse.setErrorMsg("Error creating new User " + personName);
        }
        return xresponse;
    }

    public String createThumbnailString(String imgStr) throws UnsupportedEncodingException, IOException {

        try {
            byte[] imageByte = Base64.getDecoder().decode(new String(imgStr).getBytes("UTF-8"));
            InputStream input = new ByteArrayInputStream(imageByte);
            BufferedImage profileImg = ImageIO.read(input);
            BufferedImage thumbnail = Scalr.resize(profileImg, Scalr.Method.QUALITY, 200);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(thumbnail, "jpeg", baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            String thumbnailStr = java.util.Base64.getEncoder().encodeToString(imageInByte);
            return thumbnailStr;
        } catch (Exception ex) {
            return "";
        }

    }

    public List<UserTable> getAllUsers(RegisteredAccount myAccount) {
        List<UserTable> userList = userInfoDao.getAllUsers(myAccount);
        return userList;
    }

    public static String createNewPersonwithDescriptor(String baseUrl, String personName) throws MalformedURLException, JSONException, KeyManagementException {

        HttpsTrustManager.allowAllSSL();
        String api1 = baseUrl + "/storage/persons";
        URL object = new URL(api1);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestProperty("Content-Type", "application/json");
            //con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            JSONObject request = new JSONObject();
            request.put("user_data", personName);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(request.toString());
            wr.flush();

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_CREATED) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

    private String extractImageDescriptor(String baseUrl, String imgStr) throws MalformedURLException, KeyManagementException {

        HttpsTrustManager.allowAllSSL();
        String api2 = baseUrl + "/storage/descriptors";
        URL object = new URL(api2);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestProperty("Content-Type", "image/png");
            //con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            byte[] imageByte = Base64.getDecoder().decode(new String(imgStr).getBytes("UTF-8"));
            InputStream in = new ByteArrayInputStream(imageByte);

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            copy(in, con.getOutputStream());
            wr.flush();
            wr.close();

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_CREATED) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

    private String attachDescriptorIdtoPerson(String baseUrl, String person_id, String id) throws MalformedURLException, KeyManagementException {

        HttpsTrustManager.allowAllSSL();
        String api1 = baseUrl + "/storage/persons/" + person_id + "/linked_descriptors?descriptor_id=" + id + "&do=attach";
        URL object = new URL(api1);
        String lunaResponse = "";

        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
            methodsField.setAccessible(true);
            // get the methods field modifiers
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            // bypass the "private" modifier 
            modifiersField.setAccessible(true);

            // remove the "final" modifier
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            /* valid HTTP methods */
            String[] methods = {
                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
            };
            // set the new methods - including patch
            methodsField.set(null, methods);

        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
            return "API_ERROR";
        }

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            //con.setRequestProperty("Content-Type", "application/json");
            //con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("PATCH");

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

    public XResponse delUser(String personId, RegisteredAccount myAccount) throws MalformedURLException, KeyManagementException {

        XResponse xresponse = new XResponse();
        String userName;

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(myAccount);
        UserInfo user = getUserbyPersonId(personId);
        if (user != null) {
            userName = user.getFirstName() + " " + user.getLastName();
        } else {
            userName = personId;
        }

        if (serverSettings == null) {
            log.error("No server Settings found for Account " + myAccount.getEmail());
            xresponse.setErrorCode(1);
            xresponse.setErrorMsg("No server Settings found for Account " + myAccount.getEmail());
            return xresponse;
        }

        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + "/3";
        String ApiResponse = delPersonWithDescriptorId(baseUrl, personId);
        if (ApiResponse.equals("API_ERROR")) {
            xresponse.setErrorCode(1);
            xresponse.setErrorMsg("Error Deleting User " + userName + "  from Luna Server");
        } else {
            boolean delResult = userInfoDao.deleteUser(personId);
            if (delResult) {
                xresponse.setErrorCode(0);
                xresponse.setErrorMsg("Successfully deleted User " + userName);
            } else {
                xresponse.setErrorCode(1);
                xresponse.setErrorMsg("Error deleting User " + userName);
            }
        }

        return xresponse;
    }

    private String delPersonWithDescriptorId(String baseUrl, String person_id) throws MalformedURLException, KeyManagementException {

        HttpsTrustManager.allowAllSSL();
        String api1 = baseUrl + "/storage/persons/" + person_id;
        URL object = new URL(api1);
        String lunaResponse = "";

        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
            methodsField.setAccessible(true);
            // get the methods field modifiers
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            // bypass the "private" modifier 
            modifiersField.setAccessible(true);

            // remove the "final" modifier
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            /* valid HTTP methods */
            String[] methods = {
                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
            };
            // set the new methods - including patch
            methodsField.set(null, methods);

        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
            return "API_ERROR";
        }

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            //con.setRequestProperty("Content-Type", "application/json");
            //con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("DELETE");

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

    private String attachFSListIdtoPerson(String baseUrl, String person_id, String fslistId) throws KeyManagementException, MalformedURLException {
        HttpsTrustManager.allowAllSSL();
        String api = baseUrl + "/storage/persons/" + person_id + "/linked_lists?list_id=" + fslistId + "&do=attach";
        URL object = new URL(api);
        String lunaResponse = "";

        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
            methodsField.setAccessible(true);
            // get the methods field modifiers
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            // bypass the "private" modifier 
            modifiersField.setAccessible(true);

            // remove the "final" modifier
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            /* valid HTTP methods */
            String[] methods = {
                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
            };
            // set the new methods - including patch
            methodsField.set(null, methods);

        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
            return "API_ERROR";
        }

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            //con.setRequestProperty("Content-Type", "application/json");
            //con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("PATCH");

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

    public String createLunaToken(RegisteredAccount myAccount, String vidcamid) throws KeyManagementException, MalformedURLException, JSONException {

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(myAccount);

        if (serverSettings == null) {
            log.error("No server Settings found for Account " + myAccount.getEmail());
            return "API_ERROR";
        }

        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + "/3";
        HttpsTrustManager.allowAllSSL();
        String api1 = baseUrl + "/account/tokens";
        URL object = new URL(api1);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");

            JSONObject request = new JSONObject();
            request.put("token_data", vidcamid);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(request.toString());
            wr.flush();

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_CREATED) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

    public String delLunaToken(RegisteredAccount myAccount, CameraInfo camInfo) throws KeyManagementException, MalformedURLException, JSONException {

        //First get the serversettings to figure out baseURL
        ServerSettings serverSettings = serverSettingsDao.getServerSettings(myAccount);

        if (serverSettings == null) {
            log.error("No server Settings found for Account " + myAccount.getEmail());
            return "API_ERROR";
        }

        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
            methodsField.setAccessible(true);
            // get the methods field modifiers
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            // bypass the "private" modifier 
            modifiersField.setAccessible(true);

            // remove the "final" modifier
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            /* valid HTTP methods */
            String[] methods = {
                "GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH"
            };
            // set the new methods - including patch
            methodsField.set(null, methods);

        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
            return "API_ERROR";
        }

        String baseUrl = "https://" + serverSettings.getServerip() + ":" + serverSettings.getServerport() + "/3";
        HttpsTrustManager.allowAllSSL();
        String api1 = baseUrl + "/account/tokens";
        URL object = new URL(api1);
        String lunaResponse = "";

        try {
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            String basicAuth = "Basic ZGVtb0BkZW1vLmRlbW86REVNT19URVNU";
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("DELETE");

            JSONObject request = new JSONObject();
            ArrayList tokenList = new ArrayList();
            tokenList.add(camInfo.getToken());
            //String TokenListString = tokenList.toString();
            request.put("tokens", tokenList);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(request.toString());
            wr.flush();

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_NO_CONTENT) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                //System.out.println("" + sb.toString());
                lunaResponse = sb.toString();
            } else {
                lunaResponse = "API_ERROR";
            }

        } catch (IOException e) {
            return "API_ERROR";
        }

        return lunaResponse;
    }

}
