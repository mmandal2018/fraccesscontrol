/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kmbic.admin.exceptions;

/**
 *
 * 
 */
public class AdminException extends Exception {
     public enum ERROR_CODES {userNotFound, internalError, registerUserFailed, updateRiskFailed};

    public  AdminException (ERROR_CODES error) {
        super(error.name());
    }
     
}
