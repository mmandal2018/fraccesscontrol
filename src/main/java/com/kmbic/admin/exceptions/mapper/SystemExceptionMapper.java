package com.kmbic.admin.exceptions.mapper;

import com.kmbic.admin.exceptions.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Locale;

@Provider
@Component
public class SystemExceptionMapper implements ExceptionMapper<SystemException> {

    @Autowired
    MessageSource messageResource;

    @Override
    public Response toResponse(SystemException e) {

        return Response.serverError().entity(messageResource.getMessage("internalError", null, Locale.getDefault())).build();
    }
}
