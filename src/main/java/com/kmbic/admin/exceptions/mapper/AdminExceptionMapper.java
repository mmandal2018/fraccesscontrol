/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kmbic.admin.exceptions.mapper;

import com.kmbic.admin.exceptions.AdminException;
import java.util.Locale;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

/**
 *
 * 
 */
@Provider
@Component
public class AdminExceptionMapper  implements ExceptionMapper<AdminException> {
    @Autowired
    MessageSource messageResource;

    @Override
    public Response toResponse(AdminException e) {
        
        String message = messageResource.getMessage(e.getMessage(), null, Locale.getDefault());
        return Response.serverError().entity(messageResource.getMessage(e.getMessage(), null, Locale.getDefault())).build();
    }
}
