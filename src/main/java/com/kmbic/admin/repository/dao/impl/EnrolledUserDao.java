/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.repository.dao.impl;

import com.kmbic.admin.representations.RegisteredAccount;
import com.kmbic.admin.representations.UserInfo;
import com.kmbic.admin.representations.UserTable;
import com.kmbic.admin.representations.XResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

/**
 *
 *
 */
public class EnrolledUserDao extends DaoBase {

    static Logger log = Logger.getLogger(EnrolledUserDao.class.getName());
    MongoCollection users;

    public EnrolledUserDao(Jongo jongo) {
        super(jongo);
    }

    protected MongoCollection getUserCollection() {
        if (users == null) {
            users = getCollection(USERS_COLLECTION);
        }
        return users;

    }

    public XResponse enrollUser(UserInfo userInfo) {
        XResponse response = new XResponse();
        try {
            MongoCollection userColl = getUserCollection();
            UserInfo user = userColl.findOne("{firstName: #, lastName : #, accountemail : #}", userInfo.getFirstName(), userInfo.getLastName(), userInfo.getAccountemail())
                    .as(UserInfo.class);
            if (user != null) {
                response.setErrorCode(1);
                response.setErrorMsg("User " + userInfo.getFirstName() + " " + userInfo.getLastName() + " already exists");
                return response;
            }
            // User is null

            userColl.insert(userInfo);
            response.setErrorCode(0);
            response.setErrorMsg("Successfully added new User " + userInfo.getFirstName() + " " + userInfo.getLastName());
            return response;

        } catch (Exception ex) {
            log.error("Error in adding New User" + ex);
            response.setErrorCode(1);
            response.setErrorMsg("Error in adding new User");
            return response;
        }
    }

    public List<UserTable> getAllUsers(RegisteredAccount myAccount) {
        List<UserTable> userInfoList = new ArrayList();
        MongoCollection userColl = getUserCollection();
        Iterable<UserTable> userIterable;
        userIterable = userColl.aggregate("{$match : {accountemail : # }}", myAccount.getEmail())
                .and("{$project : {registrationTime : 1,  Name : {$concat: ['$firstName', ' ' , '$lastName' ]},  group : 1, "
                        + "employeeId : 1,  thumbnail: 1, person_id:1 }}")
                .as(UserTable.class);

        Iterator<UserTable> userItr = userIterable.iterator();
        while (userItr.hasNext()) {
            UserTable user = userItr.next();
            userInfoList.add(user);
        }

        return userInfoList;
    }

    public boolean deleteUser(String personId) {

        try {
            MongoCollection userColl = getUserCollection();
            userColl.remove("{person_id : #}", personId);

            return true;
        } catch (Exception ex) {
            log.error("Error Deleting User " + personId + " " + ex);
            return false;
        }

    }

    public UserInfo getUserbyPersonId(String personId) {
        UserInfo user = null;

        try {
            MongoCollection userColl = getUserCollection();
            user = userColl.findOne("{person_id: #}", personId)
                    .as(UserInfo.class);

        } catch (Exception ex) {
            return user;
        }

        return user;
    }

}
