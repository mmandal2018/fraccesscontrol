/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.repository.dao.impl;


import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;


/**
 *
 *
 */
public class ReportsDao extends DaoBase {

    static Logger log = Logger.getLogger(ReportsDao.class.getName());
    MongoCollection xusers;

    public ReportsDao(Jongo jongo) {
        super(jongo);
    }

    protected MongoCollection getUserCollection() {
        if (xusers == null) {
            xusers = getCollection(USERS_COLLECTION);
        }
        return xusers;

    }

}
