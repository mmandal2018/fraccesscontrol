/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.repository.dao.impl;

import com.kmbic.admin.representations.CameraList;
import com.kmbic.admin.representations.CameraInfo;
import com.kmbic.admin.representations.RegisteredAccount;
import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import com.mongodb.WriteResult;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 *
 */
public class AccountInfoDao extends DaoBase {

    static Logger log = Logger.getLogger(AccountInfoDao.class.getName());
    private MongoCollection accountInfo;

    public AccountInfoDao(Jongo jongo) {
        super(jongo);
        this.accountInfo = jongo.getCollection(ACCOUNTS_COLLECTION);
    }

    protected MongoCollection getAccountsCollection() {
        if (accountInfo == null) {
            this.accountInfo = jongo.getCollection(ACCOUNTS_COLLECTION);
        }
        return accountInfo;

    }

    public boolean saveCameraInfo(CameraInfo camInfo, RegisteredAccount account) {

        camInfo.setUpdateTime(new Date());
        WriteResult saveResult;
        List<CameraInfo> camInfoList = null;
        CameraList cameraList;

        if (account.getLinkedCameras() == null) {
            cameraList = new CameraList();
            camInfoList = new ArrayList();
            camInfo.setCamId(1);
            camInfo.setFrstatus("Stopped");
            camInfoList.add(camInfo);
            cameraList.setNumcameras(1);
            cameraList.setLastcamId(1);
            cameraList.setCaminfo(camInfoList);
            account.setLinkedCameras(cameraList);
        } else {
            cameraList = account.getLinkedCameras();
            camInfoList = cameraList.getCaminfo();

            if (camInfoList != null) {
                camInfo.setCamId(cameraList.getLastcamId() + 1);
                camInfo.setFrstatus("Stopped");
                camInfoList.add(camInfo);
                cameraList.setNumcameras(cameraList.getNumcameras() + 1);
                cameraList.setLastcamId(cameraList.getLastcamId() + 1);
                cameraList.setCaminfo(camInfoList);
                account.setLinkedCameras(cameraList);
            } else { //camSettingsList is null
                camInfoList = new ArrayList();
                camInfo.setCamId(1);
                camInfoList.add(camInfo);
                cameraList.setNumcameras(1);
                cameraList.setLastcamId(1);
                cameraList.setCaminfo(camInfoList);
                account.setLinkedCameras(cameraList);
            }
        }

        try {
            MongoCollection accountColl = getAccountsCollection();
            accountColl.update("{email:#}", account.getEmail())
                    .with("{ $set: { linkedCameras: #}}", account.getLinkedCameras());
            return true;
        } catch (Exception ex) {
            log.error("Error updating Camera List for  " + account.getEmail() + " " + ex.getMessage());
            return false;

        }
    }

    public CameraList getCameraInfoList(RegisteredAccount account) {

        RegisteredAccount myaccount = null;
        CameraList camList = null;

        try {
            MongoCollection accountColl = getAccountsCollection();
            myaccount = accountColl.findOne("{email: #}", account.getEmail()).as(RegisteredAccount.class);
        } catch (Exception ex) {
            log.error("No settings found for " + account.getEmail() + ex);

        }
        if (myaccount == null) {
            log.error("No settings found for " + account.getEmail());
        } else {
            camList = myaccount.getLinkedCameras();
        }
        return camList;
    }

    public RegisteredAccount getAccountInfobyEmail(String email) {
        RegisteredAccount accountinfo = null;
        try {
            MongoCollection accountColl = getAccountsCollection();
            accountinfo = accountColl.findOne("{email: # }", email).as(RegisteredAccount.class);
        } catch (Exception ex) {
            log.error("Account not Found for " + email + ex);
        }
        if (accountinfo == null) {
            log.error("Account not Found for " + email);
        }
        return accountinfo;
    }

    public RegisteredAccount saveAccountInfo(RegisteredAccount newAccount) {

        try {
            MongoCollection accountColl = getAccountsCollection();
            accountColl.insert(newAccount);
        } catch (Exception ex) {
            log.error("Error inserting new Account");
        }

        return newAccount;
    }

    public boolean updateCameraInfo(String emailAddress, CameraInfo caminfo) {
        boolean result = false;
        try {
            MongoCollection accountColl = getAccountsCollection();
            accountColl.update("{email:#, linkedCameras.caminfo.camId:#}", emailAddress, caminfo.getCamId())
                    .with("{$set: {linkedCameras.caminfo.$.vidcamid: #, linkedCameras.caminfo.$.vidcamip: #, "
                            + "linkedCameras.caminfo.$.vidcamport: #, linkedCameras.caminfo.$.vidcamprotocol: #, linkedCameras.caminfo.$.vidcamstreampath: #,"
                            + "linkedCameras.caminfo.$.vidcamuserid: #, linkedCameras.caminfo.$.vidcampwd: # }}", caminfo.getVidcamid(),
                            caminfo.getVidcamip(), caminfo.getVidcamport(), caminfo.getVidcamprotocol(), caminfo.getVidcamstreampath(),
                            caminfo.getVidcamuserid(), caminfo.getVidcampwd());
            return true;
        } catch (Exception ex) {
            log.error("Error updating Camera Info for  " + emailAddress + " " + ex.getMessage());
            return false;

        }

    }

    public boolean deleteCamera(String emailAddress, CameraInfo caminfo) {
        boolean result = false;
        try {
            MongoCollection accountColl = getAccountsCollection();
            RegisteredAccount myaccount = accountColl.findAndModify("{email:#}", emailAddress)
                    .with("{$pull: {linkedCameras.caminfo:{camId:#}}}", caminfo.getCamId())
                    .as(RegisteredAccount.class);
            if (myaccount != null) {
                int numCameras = myaccount.getLinkedCameras().getNumcameras() - 1;
                accountColl.update("{email:#}", emailAddress)
                        .with("{$set: {linkedCameras.numcameras: #}}", numCameras);
                return true;
            } else {
                return false;
            }

        } catch (Exception ex) {
            log.error("Error deleting Camera Info for  " + emailAddress + " " + ex.getMessage());
            return false;

        }

    }

    public boolean updateCameraFRandPIDStatus(String emailAddress, CameraInfo caminfo) {
        boolean result = false;
        try {
            MongoCollection accountColl = getAccountsCollection();
            accountColl.update("{email:#, linkedCameras.caminfo.camId:#}", emailAddress, caminfo.getCamId())
                    .with("{$set: {linkedCameras.caminfo.$.frstatus: #, linkedCameras.caminfo.$.clientpid: # }}", caminfo.getFrstatus(), caminfo.getClientpid());
            return true;
        } catch (Exception ex) {
            log.error("Error updating Camera FR and PID Status for  " + emailAddress + " " + ex.getMessage());
            return false;

        }

    }

    public boolean updateClientPID(String emailAddress, CameraInfo caminfo) {

        try {
            MongoCollection accountColl = getAccountsCollection();
            accountColl.update("{email:#, linkedCameras.caminfo.camId:#}", emailAddress, caminfo.getCamId())
                    .with("{$set: {linkedCameras.caminfo.$.clientpid: # }}", caminfo.getClientpid());
            return true;
        } catch (Exception ex) {
            log.error("Error updating Camera Info for  " + emailAddress + " " + ex.getMessage());
            return false;
        }

    }

    public CameraInfo getCameraInfobyID(String email, int camId) {
        CameraInfo camInfo = null;
        try {
            MongoCollection accountsColl = getAccountsCollection();
            RegisteredAccount aInfo = accountsColl.findOne("{email: #}", email).as(RegisteredAccount.class);

            if (aInfo != null) {
                CameraList cameraList = aInfo.getLinkedCameras();
                if (cameraList != null) {
                    List<CameraInfo> camInfoList = cameraList.getCaminfo();
                    for (int i = 0; i < camInfoList.size(); i++) {
                        if (camInfoList.get(i).getCamId() == camId) {
                            camInfo = camInfoList.get(i);
                        }

                    }
                }
            }
        } catch (Exception ex) {
            log.error("No account found for " + email + ex);
            return camInfo;
        }

        return camInfo;
    }

    public CameraInfo getCameraInfobyName(String email, String camName) {
        CameraInfo camInfo = null;
        try {
            MongoCollection accountsColl = getAccountsCollection();
            RegisteredAccount aInfo = accountsColl.findOne("{email: #}", email).as(RegisteredAccount.class);

            if (aInfo != null) {
                CameraList cameraList = aInfo.getLinkedCameras();
                if (cameraList != null) {
                    List<CameraInfo> camInfoList = cameraList.getCaminfo();
                    for (int i = 0; i < camInfoList.size(); i++) {
                        if (camInfoList.get(i).getVidcamid().equalsIgnoreCase(camName)) {
                            camInfo = camInfoList.get(i);
                        }

                    }
                }
            }
        } catch (Exception ex) {
            log.error("No account found for " + email + ex);
            return camInfo;
        }

        return camInfo;
    }

}
