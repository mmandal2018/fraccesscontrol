/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.repository.dao.impl;

import com.kmbic.admin.representations.RegisteredAccount;
import org.apache.log4j.Logger;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import com.kmbic.admin.representations.ServerSettings;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;
import java.util.Date;

/**
 *
 *
 */
public class ServerSettingsDao extends DaoBase {

    static Logger log = Logger.getLogger(ServerSettingsDao.class.getName());
    private MongoCollection serverSettings;

    public ServerSettingsDao(Jongo jongo) {
        super(jongo);
        this.serverSettings = jongo.getCollection(SERVER_SETTINGS_COLLECTION);
    }

    protected MongoCollection getServerSettingsCollection() {
        if (serverSettings == null) {
            this.serverSettings = jongo.getCollection(SERVER_SETTINGS_COLLECTION);
        }
        return serverSettings;

    }

    public boolean saveServerSettings(ServerSettings settings, RegisteredAccount account) {

        settings.setUpdateTime(new Date());
        WriteResult saveResult;
        MongoCollection settingsColl = getServerSettingsCollection();
        ServerSettings savedSettings = settingsColl.findAndModify("{email: #}", account.getEmail())
                .with("{$set : {updateTime : #, serverip : #,serverport : #, serverendpoint : #,"
                        + "deffsclientdir: #, fsListId : #, moxaip : #, dooreleasetime :# }}",
                        settings.getUpdateTime(), settings.getServerip(), settings.getServerport(),
                        settings.getServerendpoint(), settings.getDeffsclientdir(), settings.getFsListId(),
                        settings.getMoxaip(), settings.getDoorreleasetime())
                .as(ServerSettings.class);

        if (savedSettings == null) {
            try {
                settings.setEmail(account.getEmail());
                saveResult = settingsColl.insert(settings);
            } catch (MongoException ex) {
                log.error("Insert failed for" + account.getEmail(), ex);
                return false;
            }
        }

        return true;
    }

    public ServerSettings getServerSettings(RegisteredAccount account) {
        ServerSettings settingsinfo = null;
        try {
            MongoCollection settingsColl = getServerSettingsCollection();
            settingsinfo = settingsColl.findOne("{email: #}", account.getEmail()).as(ServerSettings.class);
        } catch (Exception ex) {
            log.error("No settings found for " + account.getEmail() + ex);
        }
        if (settingsinfo == null) {
            log.error("No settings found for " + account.getEmail());
        }
        return settingsinfo;
    }

}
