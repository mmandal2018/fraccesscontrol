package com.kmbic.admin.representations;

import java.util.ArrayList;

/**
 *
 *
 */
public class WeeklyStats {

    public ArrayList<String> matchdate;
    public ArrayList<Integer> matchVal;
    public ArrayList<String> mismatchdate;
    public ArrayList<Integer> mismatchVal;
    
    

    public ArrayList<String> getMatchdate() {
        return matchdate;
    }

    public void setMatchdate(ArrayList<String> matchdate) {
        this.matchdate = matchdate;
    }

    public ArrayList<String> getMismatchdate() {
        return mismatchdate;
    }

    public void setMismatchdate(ArrayList<String> mismatchdate) {
        this.mismatchdate = mismatchdate;
    }

    public ArrayList<String> getMatchDate() {
        return matchdate;
    }

    public void setMatchDate(ArrayList<String> date) {
        this.matchdate = date;
    }

    public ArrayList<Integer> getMatchVal() {
        return matchVal;
    }

    public void setMatchVal(ArrayList<Integer> matchVal) {
        this.matchVal = matchVal;
    }

    public ArrayList<Integer> getMismatchVal() {
        return mismatchVal;
    }

    public void setMismatchVal(ArrayList<Integer> mismatchVal) {
        this.mismatchVal = mismatchVal;
    }

}
