package com.kmbic.admin.representations;

/**
 *
 *
 */
public class EventResponse {

    public static final int SUCCESS = 0;
    public static final int FAILED = 1;
    public int eventNum;
    public String eventTime;
    private int errorCode;
    private String name;
    private String imageStr; // Needs to be base64 encoded
    private String enrolledImageStr; // Needs to be base64 encoded
    private String camId;
    private Double matchPercentage;

    public String getCamId() {
        return camId;
    }

    public void setCamId(String camId) {
        this.camId = camId;
    }

    public Double getMatchPercentage() {
        return matchPercentage;
    }

    public void setMatchPercentage(Double matchPercentage) {
        this.matchPercentage = matchPercentage;
    }

    public int getEventNum() {
        return eventNum;
    }

    public void setEventNum(int eventNum) {
        this.eventNum = eventNum;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageStr() {
        return imageStr;
    }

    public void setImageStr(String imageStr) {
        this.imageStr = imageStr;
    }

    public String getEnrolledImageStr() {
        return enrolledImageStr;
    }

    public void setEnrolledImageStr(String enrolledImageStr) {
        this.enrolledImageStr = enrolledImageStr;
    }

    public String getAuthorized() {
        return authorized;
    }

    public void setAuthorized(String authorized) {
        this.authorized = authorized;
    }
    private String authorized;

}
