package com.kmbic.admin.representations;

import java.util.Date;
import java.util.List;

/**
 *
 *
 */
public class EnrolledUser {

   private String id;
   private List<String> devid;
   private String usrrisk;
   private String loc;
   private Date regDate;
   private String regDateStr;
   private Date lastLogin;
   private String lstlog;
   private String frr;
   private String flagged;
   //private String frrper;
   //private String far;
   private String tlogins;
   private String trk;
   private boolean isBlackList;
   private boolean isWhiteList;
   private String numDevices;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public List<String> getDevid() {
      return devid;
   }

   public void setDevid(List<String> devid) {
      this.devid = devid;
   }

   public String getUsrrisk() {
      return usrrisk;
   }

   public void setUsrrisk(String usrrisk) {
      this.usrrisk = usrrisk;
   }

   public String getLoc() {
      return loc;
   }

   public void setLoc(String loc) {
      this.loc = loc;
   }

   public Date getLastLogin() {
      return lastLogin;
   }

   public void setLastLogin(Date lastLogin) {
      this.lastLogin = lastLogin;
   }

   public Date getRegDate() {
      return regDate;
   }

   public void setRegDate(Date date) {
      this.regDate = date;
   }

   public String getRegDateStr() {
      return regDateStr;
   }

   public void setRegDateStr(String dateStr) {
      this.regDateStr = dateStr;
   }

   /*
   public String getFrejectper() {
      return frrper;
   }

   public void setFrejectper(String frrper) {
      this.frrper = frrper;
   }

    */
   public String getLstlog() {
      return lstlog;
   }

   public void setLstlog(String lstlog) {
      this.lstlog = lstlog;
   }

   public String getFreject() {
      return frr;
   }

   public void setFreject(String frr) {
      this.frr = frr;
   }

   public String getFlagged() {
      return flagged;
   }

   public void setFlagged(String flagged) {
      this.flagged = flagged;
   }

   public String getLogins() {
      return tlogins;
   }

   public void setLogins(String tlogins) {
      this.tlogins = tlogins;
   }

   /*
   public String getFaccept() {
      return far;
   }

   public void setFaccept(String far) {
      this.far = far;
   }

    */
   public String getTrk() {
      if (trk == null) {
         trk = "n";
      }
      return trk;
   }

   public void setTrk(String trk) {
      this.trk = trk;
   }

   public String getNumDevices() {
      return numDevices;
   }

   public void setNumDevices(String numDevices) {
      this.numDevices = numDevices;
   }

   public boolean isIsBlackList() {
      return isBlackList;
   }

   public void setIsBlackList(boolean isBlackList) {
      this.isBlackList = isBlackList;
   }

   public boolean isIsWhiteList() {
      return isWhiteList;
   }

   public void setIsWhiteList(boolean isWhiteList) {
      this.isWhiteList = isWhiteList;
   }

}
