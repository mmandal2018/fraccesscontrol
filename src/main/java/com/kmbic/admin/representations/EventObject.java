package com.kmbic.admin.representations;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class EventObject implements Delayed {

    String eventName;
    String eventStatus;
    long eventStartTime;
    long eventReleaseTime;

    public EventObject(String eventName, String eventStatus, long eventReleaseTime) {
        this.eventName = eventName;
        this.eventStatus = eventStatus;
        this.eventStartTime = System.currentTimeMillis();
        this.eventReleaseTime = System.currentTimeMillis() + eventReleaseTime;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long diff = eventReleaseTime - (System.currentTimeMillis());
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed eventObj) {

        if(this.eventReleaseTime < ((EventObject) eventObj).eventReleaseTime) {
            return -1;
        }
        else if(this.eventReleaseTime > ((EventObject) eventObj).eventReleaseTime) {
            return 1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventObject)) return false;
        EventObject that = (EventObject) o;
        return eventStartTime == that.eventStartTime &&
                eventReleaseTime == that.eventReleaseTime &&
                Objects.equals(eventName, that.eventName) &&
                Objects.equals(eventStatus, that.eventStatus);
    }

    @Override
    public int hashCode() {

        return Objects.hash(eventName, eventStatus, eventStartTime, eventReleaseTime);
    }

    @Override
    public String toString() {
        return "EventObject{" +
                "eventName='" + eventName + '\'' +
                ", eventStatus='" + eventStatus + '\'' +
                ", eventStartTime=" + new Date(eventStartTime) +
                ", eventReleaseTime=" + new Date(eventReleaseTime) +
                '}';
    }
}
