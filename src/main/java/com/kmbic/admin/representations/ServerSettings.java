/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.representations;

import java.util.Date;
import org.jongo.marshall.jackson.oid.MongoId;
import org.jongo.marshall.jackson.oid.MongoObjectId;

/**
 *
 * @author kmbic
 */
public class ServerSettings {

    @MongoId
    @MongoObjectId
    String id;

    Date updateTime;
    private String name;
    private String email;

    private String serverip;
    private String serverport;
    private String serverendpoint;
    private String deffsclientdir;
    private String fsListId;

    private String moxaip;
    private String doorreleasetime;

    public String getMoxaip() {
        return moxaip;
    }

    public void setMoxaip(String moxaip) {
        this.moxaip = moxaip;
    }

    public String getDoorreleasetime() {
        return doorreleasetime;
    }

    public void setDoorreleasetime(String doorreleasetime) {
        this.doorreleasetime = doorreleasetime;
    }

    // private String fsToken;
    private String settingsErrorMsg;

    /*public String getFsToken() {
        return fsToken;
    }

    public void setFsToken(String fsToken) {
        this.fsToken = fsToken;
    }
     */
    public String getFsListId() {
        return fsListId;
    }

    public void setFsListId(String fsListId) {
        this.fsListId = fsListId;
    }

    public String getServerip() {
        return serverip;
    }

    public void setServerip(String serverip) {
        this.serverip = serverip;
    }

    public String getServerport() {
        return serverport;
    }

    public void setServerport(String serverport) {
        this.serverport = serverport;
    }

    public String getServerendpoint() {
        return serverendpoint;
    }

    public void setServerendpoint(String serverendpoint) {
        this.serverendpoint = serverendpoint;
    }

    public String getDeffsclientdir() {
        return deffsclientdir;
    }

    public void setDeffsclientdir(String deffsclientdir) {
        this.deffsclientdir = deffsclientdir;
    }

    public String getSettingsErrorMsg() {
        return settingsErrorMsg;
    }

    public void setSettingsErrorMsg(String settingsErrorMsg) {
        this.settingsErrorMsg = settingsErrorMsg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
