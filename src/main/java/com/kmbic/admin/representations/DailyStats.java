package com.kmbic.admin.representations;

/**
 *
 *
 */
public class DailyStats {

    public int identified;
    public int unidentified;

    public int getIdentified() {
        return identified;
    }

    public void setIdentified(int identified) {
        this.identified = identified;
    }

    public int getUnidentified() {
        return unidentified;
    }

    public void setUnidentified(int unidentified) {
        this.unidentified = unidentified;
    }

}
