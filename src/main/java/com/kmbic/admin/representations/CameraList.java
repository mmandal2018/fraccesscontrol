/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.representations;

import java.util.List;

/**
 *
 * @author kmbic
 */
public class CameraList {

    private int numcameras;
    private int lastcamId;
    private List<CameraInfo> caminfo;

    private String settingsErrorMsg;

    public int getLastcamId() {
        return lastcamId;
    }

    public void setLastcamId(int lastcamId) {
        this.lastcamId = lastcamId;
    }

    public int getNumcameras() {
        return numcameras;
    }

    public void setNumcameras(int numcameras) {
        this.numcameras = numcameras;
    }

    public List<CameraInfo> getCaminfo() {
        return caminfo;
    }

    public void setCaminfo(List<CameraInfo> caminfo) {
        this.caminfo = caminfo;
    }

    public String getSettingsErrorMsg() {
        return settingsErrorMsg;
    }

    public void setSettingsErrorMsg(String settingsErrorMsg) {
        this.settingsErrorMsg = settingsErrorMsg;
    }

}
