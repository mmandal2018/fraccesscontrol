/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.representations;

import java.util.List;

/**
 *
 * @author kumar
 */
public class EventResponseList {

    List<EventResponse> xresponselist;

    public List<EventResponse> getXresponselist() {
        return xresponselist;
    }

    public void setXresponselist(List<EventResponse> xresponselist) {
        this.xresponselist = xresponselist;
    }

}
