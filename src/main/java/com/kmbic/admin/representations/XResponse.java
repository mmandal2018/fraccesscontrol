/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.representations;

/**
 *
 * @author kumarv
 */
public class XResponse {
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
    
    private int errorCode;
    private String errorMsg ="";

    public XResponse() {
        this.errorCode = SUCCESS;
    }
   
    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String msg) {
        if ( msg != null )
            this.errorMsg = msg;
    }

    
}
 

