/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kmbic.admin.representations;

import java.util.Date;

/**
 *
 * @author kmbic
 */
public class CameraInfo {

    Date updateTime;
    private int camId;

    private String vidcamid;
    private String vidcamip;
    private String vidcamport;
    private String vidcamprotocol;
    private String vidcamstreampath;
    private String vidcamuserid;
    private String vidcampwd;
    private String frstatus;
    private String frclientstatus;
    private String clientpid;
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClientpid() {
        return clientpid;
    }

    public void setClientpid(String clientpid) {
        this.clientpid = clientpid;
    }

    public String getFrstatus() {
        return frstatus;
    }

    public void setFrstatus(String frstatus) {
        this.frstatus = frstatus;
    }

    public String getFrclientstatus() {
        return frclientstatus;
    }

    public void setFrclientstatus(String frclientstatus) {
        this.frclientstatus = frclientstatus;
    }

    public String getVidcamid() {
        return vidcamid;
    }

    public void setVidcamid(String vidcamid) {
        this.vidcamid = vidcamid;
    }

    public String getVidcamip() {
        return vidcamip;
    }

    public void setVidcamip(String vidcamip) {
        this.vidcamip = vidcamip;
    }

    public String getVidcamport() {
        return vidcamport;
    }

    public void setVidcamport(String vidcamport) {
        this.vidcamport = vidcamport;
    }

    public String getVidcamprotocol() {
        return vidcamprotocol;
    }

    public void setVidcamprotocol(String vidcamprotocol) {
        this.vidcamprotocol = vidcamprotocol;
    }

    public String getVidcamstreampath() {
        return vidcamstreampath;
    }

    public void setVidcamstreampath(String vidcamstreampath) {
        this.vidcamstreampath = vidcamstreampath;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getVidcamuserid() {
        return vidcamuserid;
    }

    public void setVidcamuserid(String vidcamuserid) {
        this.vidcamuserid = vidcamuserid;
    }

    public String getVidcampwd() {
        return vidcampwd;
    }

    public void setVidcampwd(String vidcampwd) {
        this.vidcampwd = vidcampwd;
    }

    public int getCamId() {
        return camId;
    }

    public void setCamId(int camId) {
        this.camId = camId;
    }

}
