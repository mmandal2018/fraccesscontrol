
if("MxDebugIE"in window&&document.all){document.documentElement.debug="true";document.write('<script language="javascript" type="text/javascript" src="/control/firebug/firebug.js"><\/script>');}else if(!("console"in window)){console=new Object();console.log=console.debug=console.info=console.warn=console.error=function(){};console.error=alert;}
var MxJsLib=(function(){var URLCOUNTER=Math.floor(Math.random()*10000000);var self={URLNoRecordingsImage:'/decor/m1m-error.jpg',URLNoRecordingsImage_PDA:'/decor/m1m-error_PDA.jpg',URLNoImagesImage:'/decor/err_no_images.jpg',URLNoImagesImage_PDA:'/decor/err_no_images_PDA.jpg',URLDummyCameraImage:'/decor/logo-200.jpg',URLDummyCameraImage_PDA:'/decor/logo-200.jpg',registerBodyOnloadHandler:function(onloadHandlerFunction){if(typeof onloadHandlerFunction!='undefined'){var oldOnloadHandlerFunction=window.onload;if(typeof(oldOnloadHandlerFunction)!='function'){window.onload=onloadHandlerFunction;}else{window.onload=function(){oldOnloadHandlerFunction();onloadHandlerFunction();}}}},registerBodyOnunloadHandler:function(FuncRef){if(typeof FuncRef!='undefined'){var oldHandlerFunction=window.onunload;if(typeof(oldHandlerFunction)!='function'){window.onunload=FuncRef;}else{window.onunload=function(){oldHandlerFunction();FuncRef();}}}},clearNode:function(node){if(!node)return;while(node.hasChildNodes()){self.discardElement(node.firstChild);}},discardElement:function(element){if(element.parentNode){element.parentNode.removeChild(element);}
if(!document.all){return;}
var garbageBin=document.getElementById('IELeakGarbageBin');if(!garbageBin){garbageBin=document.createElement('DIV');garbageBin.id='IELeakGarbageBin';garbageBin.style.display='none';document.body.appendChild(garbageBin);}
garbageBin.appendChild(element);garbageBin.innerHTML='';},makeClosureToCallObjectMethod:function(obj,fun){return function(){obj[fun]()};},Exception:function(msg){this.message=msg;},makeUncachedURL:function(path){if(path.substr(-1,1)=="&"){;}else if(path.indexOf("?")<0){path+="?";}else{path+="&";}
return path+(URLCOUNTER++);},convertAlarmAddress:function(Address){if(typeof Address!="string"){Address=Address.toString();}
if(Address==''){throw new MxJsLib.Exception("Address must not be empty.");}
if(Address.indexOf(".")==-1){Address+=".0";}
var AddressArray=Address.split(".",2);if(AddressArray[0]<0||AddressArray[1]<0){throw new MxJsLib.Exception("Address '"+Address+"' must not be negative.");}
if(isNaN(AddressArray[0])||isNaN(AddressArray[1])){throw new MxJsLib.Exception("Address '"+Address+"' must be numerical.");}
return AddressArray;},openCenteredPopup:function(URL,Name,Options){if(!Options){Options={width:200,height:200};}
var Position=self.calcPopupPositionOnWindow(window,null,Options.width,Options.height);if(Position){Options['left']=Position[0];Options['top']=Position[1];}
var OptionsString="";for(var i in Options){if(OptionsString.length>0){OptionsString+=",";}
OptionsString+=i+"="+Options[i];}
return window.open(URL,Name,OptionsString);},calcPopupPositionOnWindow:function(WindowReference,PopupReference,PopupWidth,PopupHeight){var wp_x=0,wp_y=0;if(WindowReference.screenLeft){if(PopupReference){PopupWidth=PopupReference.document.body.offsetWidth;PopupHeight=PopupReference.document.body.offsetHeight}
wp_x=parseInt(WindowReference.screenLeft
+WindowReference.document.body.offsetWidth/2
-PopupWidth/2);wp_y=parseInt(WindowReference.screenTop
+WindowReference.document.body.offsetHeight/2
-PopupHeight/2);return[wp_x,wp_y];}else if(WindowReference.screenX){if(PopupReference){PopupWidth=PopupReference.outerWidth;PopupHeight=PopupReference.outerWidth;}
wp_x=parseInt(WindowReference.screenX
+WindowReference.outerWidth/2
-PopupWidth/2);wp_y=parseInt(WindowReference.screenY
+WindowReference.outerHeight/2
-PopupHeight/2);return[wp_x,wp_y];}
return null;},optimizeWindowSize:function(w,h){window.resizeTo(w,h);if(window.innerHeight){var wh=window.innerHeight;var dh=document.documentElement.offsetHeight;window.resizeBy(0,dh-wh);}else if(document.compatMode&&document.compatMode=="BackCompat"&&document.body&&document.body.scrollHeight>0&&document.body.clientHeight>0){window.resizeBy(0,document.body.scrollHeight-document.body.clientHeight);}else if(document.documentElement&&document.documentElement.scrollHeight>0&&document.documentElement.clientHeight>0){window.resizeBy(0,document.documentElement.scrollHeight-document.documentElement.clientHeight);}},getScrollPositionY:function(){return window.pageYOffset||document.documentElement.scrollTop||document.body.scrollTop||0;},mergeVMWindowListTwoSections:function(A,B)
{if(!B)
return A;else if(!A)
return B;else if(A.generated!=B.generated)
return null;else if(A.hi&&(A.hi<(B.lo-1)))
return null;else if(B.hi&&(B.hi<(A.lo-1)))
return null;var merge=new Object();merge.generated=A.generated;merge.lo=(A.lo<B.lo)?A.lo:B.lo;merge.hi=(!A.hi||!B.hi)?0:(A.hi>B.hi)?A.hi:B.hi;return merge;},mergeVMWindowListSection:function(sections,newsect)
{sections[sections.length]=newsect;sections=self.sortVMWindowList(sections);for(var i=sections.length-1;i>0;i--){var mergesect=self.mergeVMWindowListTwoSections(sections[i],sections[i-1]);if(mergesect){sections[i-1]=mergesect;for(var j=i;j<sections.length-1;j++)
sections[j]=sections[j+1];sections.length--;}}
return sections;},parseVMWindowListNumber:function(string)
{var num=new Object();num.generated=false;if(string.substr(0,1)=='g'){num.generated=true;string=string.substr(1);}
num.value=parseInt(string,10);if(num.value==NaN)
num.value=0;return num;},parseVMWindowListSection:function(string)
{var nums=string.split('-');if(nums.length<=0){return null;}else{var section=new Object();var num0=self.parseVMWindowListNumber(nums[0]);section.generated=num0.generated;section.lo=num0.value;if(nums.length>1){var num1=self.parseVMWindowListNumber(nums[1]);section.generated=section.generated||num1.generated;section.hi=num1.value;}else
section.hi=num0.value;}
return section;},printVMWindowListSection:function(section)
{return((section.generated?'g':'')+(section.lo?section.lo:'')+
((section.hi>section.lo)?('-'+(section.hi?section.hi:'')):''));},sortVMWindowList:function(sections)
{function compare(a,b){return(a==b)?0:((a<b)?-1:1);};var swap=true;var first=1;while((first<sections.length)&&swap){swap=false;for(var i=sections.length-1;i>first;i--){cmp=(sections[i-1].generated==sections[i].generated)?0:(sections[i-1].generated?-1:1);if(!cmp){cmp=compare(sections[i-1].lo,sections[i].lo);if(!cmp){cmp=compare(sections[i-1].hi,sections[i].hi);if(cmp&&(!sections[i-1].hi||!sections[i].hi))
cmp=-cmp;}}
if(cmp>0){var dummy=sections[i];sections[i]=sections[i-1];sections[i-1]=dummy;swap=true;}}
first++;}
return sections;},extendVMWindowList:function(list,newid)
{if(list=='')
return newid;else{var newlist='';var sections=new Array();var elem=list.split(',');for(var i=0;i<elem.length;i++){var sect=self.parseVMWindowListSection(elem[i]);if(sect){if(sect.generated)
newlist+=(newlist!='')?',':self.printVMWindowListSection(sect);else
sections=self.mergeVMWindowListSection(sections,sect);}}
sections=self.mergeVMWindowListSection(sections,{generated:false,lo:newid,hi:newid});for(var i=0;i<sections.length;i++)
newlist+=((newlist!='')?',':'')+self.printVMWindowListSection(sections[i]);return newlist;}},getVMDefinitionsUnusedID:function(list)
{var fallback=0,rows=list.split('\n');var ids=new Array();for(var i=0;i<rows.length;i++)if((rows[i]!='')&&(rows[i].charAt(0)!='$')){var pos=rows[i].search('id=');var id=(pos>=0)?parseInt(rows[i].substr(pos+3)):-1;if((id>=0)&&!ids[id])
ids[id]=1;else
fallback++;}
var id=0;while(fallback-->=0){id++;while(ids[id])
id++;}
return id;}};self.Exception.prototype.toString=function(){if(console.trace){console.trace();}
return this.message;}
return self;})();function isFunction(a){return typeof a=='function';}
function isObject(a){return(a&&typeof a=='object')||isFunction(a);}
function isArray(a){return isObject(a)&&a.constructor==Array;}
var MxUILib=new Object();MxUILib.ElementCounter=0;MxUILib.getUniqueElementID=function(){return'MxUIElement'+(++MxUILib.ElementCounter);}
MxUILib.UIToggleController=function(button_view,initial_state){if(!button_view||!isFunction(button_view.create)||!isFunction(button_view.setState)){throw new MxJsLib.Exception("UIToggleController: view missing.");}
if(typeof initial_state=='undefined')initial_state=false;var ButtonState=!!initial_state;var CallBacks=new Array();var toggleButtonState=function(){var NewState=!ButtonState;for(var i=0;i<CallBacks.length;i++){CallBacks[i](NewState);}};var self={getState:function(){return ButtonState;},setState:function(on_off){if(arguments.length<1)
return;on_off=!!on_off;if(ButtonState!=on_off){ButtonState=on_off;button_view.setState(ButtonState);}},addToggleEventListener:function(cb_function){if(!isFunction(cb_function))return;CallBacks.push(cb_function);}};button_view.create(toggleButtonState);button_view.setState(ButtonState);return self;};MxUILib.UIButtonToggleView=function(node_id,button_html_false,button_html_true){if(!node_id||!document.getElementById(node_id)){throw new MxJsLib.Exception("UIButtonToggleView: node does not exist.");}
var ViewID="myview"+MxUILib.getUniqueElementID();var CSSClassName='';var HelpTextFalse='';var HelpTextTrue='';this.create=function(ToggleEventListener){var ButtonNode=document.createElement("button");ButtonNode.id=ViewID;ButtonNode.innerHTML="";ButtonNode.setAttribute('type','button');if(CSSClassName)ButtonNode.className=CSSClassName;document.getElementById(node_id).appendChild(ButtonNode);ButtonNode.onclick=ToggleEventListener;if(typeof ButtonNode.ondblclick=="object"){ButtonNode.ondblclick=ToggleEventListener;}};this.setState=function(state){var ButtonNode=document.getElementById(ViewID);if(state){ButtonNode.innerHTML=button_html_true;ButtonNode.title=HelpTextTrue;}else{ButtonNode.innerHTML=button_html_false;ButtonNode.title=HelpTextFalse;}};this.setBubbleHelpText=function(text_false,text_true){if(typeof text_false=='undefined')text_false='';if(typeof text_true=='undefined')text_true='';HelpTextFalse=text_false;HelpTextTrue=text_true;};this.setCSSClass=function(class_name){if(typeof class_name=='undefined')return;CSSClassName=class_name;var ButtonNode=document.getElementById(ViewID);if(ButtonNode)ButtonNode.className=CSSClassName;};};MxUILib.createCenteredDiv=function(w,h){var OuterDiv=document.createElement("DIV");OuterDiv.style.position="absolute";OuterDiv.style.top=(150+MxJsLib.getScrollPositionY())+"px";OuterDiv.style.left="0px";OuterDiv.style.right="0px";var InnerDiv=document.createElement("DIV");if(isNaN(w))w=200;InnerDiv.style.width=w+"px";if(!isNaN(h))InnerDiv.style.height=h+"px";InnerDiv.style.margin="0px auto";InnerDiv.style.backgroundColor="#EEEEEE";InnerDiv.style.border="2px solid #000088";OuterDiv.appendChild(InnerDiv);document.body.appendChild(OuterDiv);return{getNode:function(){return InnerDiv;},getNode1:function(){return OuterDiv;},close:function(){if(OuterDiv){MxJsLib.discardElement(OuterDiv);InnerDiv=OuterDiv=null;}}};};MxUILib.createBlockingDiv=function(w,h){var BlockDiv=document.createElement("DIV");BlockDiv.style.position="absolute";BlockDiv.style.top="0px";BlockDiv.style.left="0px";BlockDiv.style.right="0px";BlockDiv.style.width="100%";BlockDiv.style.height="100%";BlockDiv.style.backgroundColor="black";BlockDiv.style.opacity="0.5";BlockDiv.style.filter="alpha(opacity=50)";document.body.appendChild(BlockDiv);var ContentDiv=document.createElement("DIV");if(isNaN(w))w=200;ContentDiv.style.width=w+"px";if(!isNaN(h))ContentDiv.style.height=h+"px";ContentDiv.style.position="absolute";ContentDiv.style.top=(150+MxJsLib.getScrollPositionY())+"px";ContentDiv.style.left="0px";ContentDiv.style.right="0px";ContentDiv.style.margin="0px auto";ContentDiv.style.backgroundColor="#EEEEEE";ContentDiv.style.border="2px solid #000088";if((w!=0)||(h!=0))
document.body.appendChild(ContentDiv);return{getNode:function(){return ContentDiv;},getNode1:function(){return BlockDiv;},close:function(){if(BlockDiv){MxJsLib.discardElement(BlockDiv);BlockDiv=null;}
if(ContentDiv){MxJsLib.discardElement(ContentDiv);ContentDiv=null;}}};};MxJsLib.Cookie=function(name){this.cookiename=name;this.get=function(){var c=document.cookie;var keyvaluepair=[];var list=c.split(/;\s+/);for(var i=0;i<list.length;i++){keyvaluepair=list[i].split('=');if(keyvaluepair.length==2){if(keyvaluepair[0]==this.cookiename){return unescape(keyvaluepair[1]);}}}
return"";};this.set=function(text){if(typeof text=='undefined')text="";var c=this.cookiename+"="+escape(text);var ny=new Date();ny.setFullYear(ny.getFullYear()+1);c+='; expires='+ny.toGMTString();document.cookie=c;};}
MxJsLib.RemoteScripting=function(){this.name='RemoteScripting';this.setRequestURL=function(url){if(!url)throw new MxJsLib.Exception("RemoteScripting: url missing.");RequestURL=url;};this.setResponseListener=function(func){if(!isFunction(func))throw new MxJsLib.Exception("RemoteScripting: callback missing.");ResponseListener=func;};this.setResponseTypeJavascript=function(){ResponseTypeJavascript=true;}
this.isExecuting=function(){return(TimeoutHandler!=null);};this.execute=function(){if(!RequestURL||!ResponseListener||!IFrameElement){throw new MxJsLib.Exception("RemoteScripting: not initialized properly.");}
if(TimeoutHandler!=null)return false;var url=MxJsLib.makeUncachedURL(RequestURL);if(IFrameElement.onload==null){IFrameElement.onload=responseSuccess;IFrameElement.onerror=responseError;}
TimeoutHandler=window.setTimeout(responseTimeout,TimeoutTime*1000);if(ResponseTypeJavascript){var html='<html><header><scrip'+'t SRC=\"'+url+'\"><\/sc'+'ript><\/header><body>'+url+'<\/body><\/html>';IFrameWindow.document.open();IFrameWindow.document.write(html);IFrameWindow.document.close();}else{IFrameElement.src=url;}
return true;};this.cancel=function(){if(TimeoutHandler!=null){window.clearTimeout(TimeoutHandler);TimeoutHandler=null;IFrameElement.onload=IFrameElement.onerror=null;IFrameElement.src="";}};var MyID=MxUILib.getUniqueElementID();var RequestURL="";var ResponseListener=null;var TimeoutHandler=null;var TimeoutTime=5;var ResponseTypeJavascript=false;var responseTimeout=function(){TimeoutHandler=null;IFrameElement.onload=IFrameElement.onerror=null;IFrameElement.src="";console.log("result timeout:");ResponseListener(null);}
var responseSuccess=function(){responseHandler(true);};var responseError=function(){responseHandler(false);};var responseHandler=function(success){if(TimeoutHandler!=null){window.clearTimeout(TimeoutHandler);TimeoutHandler=null;}else{throw new MxJsLib.Exception("response while timeout");}
console.log("result:"+success);ResponseListener(IFrameWindow);};this.name+=MyID;var IFrameElement=document.createElement("iframe");IFrameElement.name=this.name;document.body.appendChild(IFrameElement);var IFrameWindow=window.frames[this.name];}