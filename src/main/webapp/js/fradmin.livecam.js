/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var cam_address = [];
var cam_id = [];
var cam_name = [];
var pic_url = [];
var streamPath = "/control/faststream.jpg?stream=full&needlength&fps=6";
//var noframepath = "/decor/err_no_images.jpg";
var noframepath = "images/err_no_images.jpg";

$('document').ready(function () {
    getCameraSettingsInfo();

});



function getCameraSettingsInfo() {
    $.getJSON('api/getCameraInfo', function (data) {
        var sdata = data;
        if (sdata.settingsErrorMsg === null) {
            var caminfo = sdata.caminfo;
            var htmlStr = '<div class="row">';

            for (var i = 0; i < caminfo.length; i++) {
                cam_address[i] = 'http://' + caminfo[i].vidcamip;
                cam_name[i] = caminfo[i].vidcamid;
                var box_title = '<h4><i class="fa fa-video-camera"></i> <span> Cam' + caminfo[i].camId + ' - ' + cam_name[i] + ' IP:' + caminfo[i].vidcamip + ' </span></h5>'
                htmlStr = htmlStr + '<div class="col-md-6 col-sm-12 col-xs-12 text-center">'
                        + '<div class="box box-info">'
                        + '<div class="box-header box-primary">'
                        + box_title
                        + '</div>'
                        + ' <div class="form-group">'
                        + '<img src="' + cam_address[i] + streamPath + '" class="video-border form-control no-border" alt=""  id="camImage' + i + '">'
                        + '</div>'
                        + '<div class="form-group">'
                        + '<a class="btn btn-app" onclick="resumeFeed(' + i + ')">'
                        + '<i class="fa fa-play"></i> Play</a>'
                        + '<a class="btn btn-app" onclick="pauseFeed(' + i + ')">'
                        + '<i class="fa fa-pause"></i> Stop</a>'
                        + '</div>'
                        + '</div> </div>';
                if (i % 2 !== 0) {
                    htmlStr = htmlStr + '</div><div class="row">';
                }

            }
        } else {
            $("#liveFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button> No Cameras Found </div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
        }

        htmlStr = htmlStr + '</div> </div>';
        $('#livecamera').html(htmlStr);


    });
}

function pauseFeed(id) {
    //$('#camImage' + id).attr('src', 'images/nostream.png');
    $('#camImage' + id).attr('src', noframepath);


}

function resumeFeed(id) {
    $('#camImage' + id).attr('src', cam_address[id] + streamPath);

}




