
$(document).ready(function () {
//$('#identified').html('<span class="info-box-number">' + 20 + '</span>');
    getDailyStats();
    getMonthlyStats();
});
function getDailyStats() {
    $.getJSON('events/getDailyStats', function (data) {
        adata = data;
        $('#identified').html('<span class="info-box-number">' + data.identified + '</span>');
        $('#unidentified').html('<span class="info-box-number">' + data.unidentified + '</span>');
    });
}

function getMonthlyStats() {
    $.getJSON('events/getWeeklyStats', function (data) {
        adata = data;
        var labelData1 = [];
        var chartData1 = [];

        var labelData2 = [];
        var chartData2 = [];

        for (var i = 0; i < data.matchdate.length; i++) {
            var rangeVal = data.matchdate[i];
            var date = moment(rangeVal);
            var dateComponent = date.utc().format('MM-DD-YY');
            var dataValue = data.matchVal[i];
            labelData1.push(dateComponent);
            chartData1.push(parseInt(dataValue));
        }

        for (var i = 0; i < data.mismatchdate.length; i++) {
            var rangeVal = data.mismatchdate[i];
            var date = moment(rangeVal);
            var dateComponent = date.utc().format('MM-DD-YY');
            var dataValue = data.mismatchVal[i];
            labelData2.push(dateComponent);
            chartData2.push(parseInt(dataValue));
        }

        var graph1Data = {

            labels: labelData1,
            datasets: [
                {
                    label: "Recognized Users",
                    backgroundColor: "rgba(75, 192, 192, 0.2)",
                    borderColor: "rgb(75, 192, 192)",
                    borderWidth: 1,
                    data: chartData1
                }]
        };
        var ctx1 = $("#matchStatsChart").get(0).getContext("2d");


        var graph1 = new Chart(ctx1, {
            type: 'bar',
            data: graph1Data,
            options: {
                responsive: true,
                legend: {
                    display: true
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                }
            }
        });


        var graph2Data = {

            labels: labelData2,
            datasets: [
                {
                    label: "UnRecognized Users",
                    backgroundColor: "rgba(255, 99, 132, 0.2)",
                    borderColor: "rgb(255, 99, 132)",
                    borderWidth: 1,
                    data: chartData2
                }]
        };

        var ctx2 = $("#matchStatsChart1").get(0).getContext("2d");

        var graph2 = new Chart(ctx2, {
            type: 'bar',
            data: graph2Data,
            options: {
                responsive: true,
                legend: {
                    display: true,

                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },

            }
        });


    });
}


        