
$('document').ready(function () {
    getServerSettingsInfo();
});


function getServerSettingsInfo() {
    $.getJSON('api/getServerSettings', function (data) {
        var sdata = data;
        if (sdata !== null) {
            $('#serverIP').val(sdata.serverip)
            $("#serverPort").val(sdata.serverport);
            $("#serverEndpoint").val(sdata.serverendpoint);
            $("#clientDir").val(sdata.deffsclientdir);
            $('#fslistid').val(sdata.fsListId);
            $('#moxaIp').val(sdata.moxaip);
            $('#doorReleaseTime').val(sdata.doorreleasetime); //$('#fstoken').val(sdata.fsToken);
        }
    });
}


$("#serversettingsform").validate({
    rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        serverIP: "required",
        serverPort: "required",
        clientDir: "required",
        fslistid: "required",
        moxaIp: "required",
        doorReleaseTime: "required"
    },

    highlight: function (element) {
        $(element).parent().addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).parent().removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'validation-error-message help-block form-helper bold',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },

    submitHandler: function (form) {
        $('.form-process').fadeIn();

        var settingsInfo = {
            updateTime: new Date(),
            name: "admin@test.com",
            serverip: $("#serverIP").val(),
            serverport: $("#serverPort").val(),
            serverendpoint: $("#serverEndpoint").val(),
            deffsclientdir: $("#clientDir").val(),
            fsListId: $("#fslistid").val(),
            moxaip: $('#moxaIp').val(),
            doorreleasetime: $('#doorReleaseTime').val()

        };

        $.ajax({
            url: "api/saveServerSettings",
            data: JSON.stringify(settingsInfo),
            type: 'POST',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                //document.getElementById("taxqueryError").innerHTML = jqXHR.responseText;
                //showLogin();
                //$('#settingsFeedback').html('<h5 class="success-msg" style:"align-center>' + jqXHR.responseText + '<h5>');

                $("#settingsFeedback").html('<div class="alert alert-info offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
                //getSettingsInfo();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('error : ' + errorThrown);
                console.log('textStatus: ' + textStatus);
                console.log('jqXHR: ' + jqXHR);
                console.log(jqXHR.responseText);
                //document.getElementById("taxqueryError").innerHTML = jqXHR.responseText;
                //$('#settingsFeedback').html('<h5 class="error-msg" style:"align-center">' + jqXHR.responseText + '<h5>');
                $("#settingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });

            }
        });
    }

});


$("#startFSClient").click(function () {
    $.ajax({
        url: "api/startFSClient",
        type: 'GET',
        success: function (data, textStatus, jqXHR) {
            $("#settingsFeedback").html('<div class="alert alert-success offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('error : ' + errorThrown);
            console.log('textStatus: ' + textStatus);
            console.log('jqXHR: ' + jqXHR);
            console.log(jqXHR.responseText);
            $("#settingsFeedback").html('<div class="alert alert-warning offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });

        }
    });
}
);

$("#stopFSClient").click(function () {
    alert('button clicked');
}
);