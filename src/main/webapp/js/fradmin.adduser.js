

var previewImg = function (event) {
    var preview = document.getElementById('previewImage');
    preview.src = URL.createObjectURL(event.target.files[0]);
}


$('document').ready(function () {
    Webcam.set({
        width: 480,
        height: 320,
        dest_width: 720,
        dest_height: 480,
        image_format: 'jpeg',
        jpeg_quality: 95
    });
    Webcam.attach('#my_camera');

});

function take_snapshot() {
    // take snapshot and get image data

    Webcam.snap(function (data_uri) {
        // display results in page
        var preview = document.getElementById('previewImage');
        preview.src = data_uri;


    });
    $("#modalwebcampic").modal('hide');
}

$("#addUserform").validate({

    rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        firstName: "required",
        lastName: "required",
        groupName: "required",
        employeeId: "required"
    },

    highlight: function (element) {
        $(element).parent().addClass('has-error');
    },
    unhighlight: function (element) {
        $(element).parent().removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'validation-error-message help-block form-helper bold',
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },

    submitHandler: function (form) {
        $('.form-process').fadeIn();

        var elem = document.getElementById('previewImage');
        if ($('#imgFile').get(0).files.length !== 0) {

            var files = document.getElementById('imgFile').files;
            var base64Image = getBase64andSubmit(files[0]);
        } else if (elem.getAttribute('src') !== "") {
            var base64Image = elem.getAttribute('src');
            submitUserData(base64Image);
        } else {
            $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + "No Image File Selected" + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
            return;
        }

    }

});


function getBase64andSubmit(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        var base64Image = reader.result;
        base64Image = base64Image.replace("data:image/jpeg;base64,", "");
        var photoList = new Array();
        photoList.push(base64Image);

        var userInfo = {
            registrationTime: new Date(),
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            group: $("#groupName").val(),
            employeeId: $("#employeeId").val(),
            phoneNum: $("#phoneNum").val(),
            photoList: photoList
        };

        $.ajax({
            url: "api/addUser",
            data: JSON.stringify(userInfo),
            type: 'POST',
            contentType: 'application/json',
            success: function (data, textStatus, jqXHR) {
                if (data.errorCode === 0) {
                    $("#userStatus").html('<div class="alert alert-info offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                    $('.alert .close').on("click", function (e) {
                        $(this).parent().fadeTo(500, 0).slideUp(500);
                    });
                } else {
                    $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                    $('.alert .close').on("click", function (e) {
                        $(this).parent().fadeTo(500, 0).slideUp(500);
                    });
                }
                resetAddUserForm();

            },
            error: function (jqXHR, textStatus, errorThrown) {

                $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
                resetAddUserForm();
            }

        });
    };
    reader.onerror = function (error) {
        $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + error + '</div>');
        $('.alert .close').on("click", function (e) {
            $(this).parent().fadeTo(500, 0).slideUp(500);
        });
    };
}





function resetAddUserForm() {
    $("#firstName").val("");
    $("#lastName").val("");
    $("#groupName").val("");
    $("#employeeId").val("");
    $("#phoneNum").val("");
    document.getElementById("imgFile").value = "";
    var preview = document.getElementById('previewImage');
    preview.src = "";
    WebcamImageURI = "";
}



function submitUserData(base64Image) {

    base64Image = base64Image.replace("data:image/jpeg;base64,", "");
    var photoList = new Array();
    photoList.push(base64Image);

    var userInfo = {
        registrationTime: new Date(),
        firstName: $("#firstName").val(),
        lastName: $("#lastName").val(),
        group: $("#groupName").val(),
        employeeId: $("#employeeId").val(),
        phoneNum: $("#phoneNum").val(),
        photoList: photoList
    };

    $.ajax({
        url: "api/addUser",
        data: JSON.stringify(userInfo),
        type: 'POST',
        contentType: 'application/json',
        success: function (data, textStatus, jqXHR) {
            if (data.errorCode === 0) {
                $("#userStatus").html('<div class="alert alert-info offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
            } else {
                $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + data.errorMsg + '</div>');
                $('.alert .close').on("click", function (e) {
                    $(this).parent().fadeTo(500, 0).slideUp(500);
                });
            }
            resetAddUserForm();

        },
        error: function (jqXHR, textStatus, errorThrown) {

            $("#userStatus").html('<div class="alert alert-danger offset4 span4"><button type="button" class="close">×</button>' + jqXHR.responseText + '</div>');
            $('.alert .close').on("click", function (e) {
                $(this).parent().fadeTo(500, 0).slideUp(500);
            });
            resetAddUserForm();
        }

    });
}


