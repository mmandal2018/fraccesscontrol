/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$('document').ready(function () {
    getLunaEvents();

});


function getLunaEvents() {

    api = 'events/getNewEvents';
    var reportsTable = $('#reportsTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                text: '<i class="fa fa-refresh fa-2x colorizedicon"></i>',
                action: function (e, dt, node, config) {
                    reportsTable.ajax.reload();
                }
            }
        ],
        'responsive': true,
        'bLengthChange': false,
        "pageLength": 10,
        "scrollCollapse": true,
        "paging": true,

        'ajax': {
            "url": api,
            "dataSrc": "xresponselist"
        },
        'columns': [
            {"data": "eventNum"},
            {"data": "eventTime"},
            {"data": "errorCode"},
            {"data": "name"},
            {"data": "imageStr"},
            {"data": "enrolledImageStr"},
            {"data": "camId"},
            {"data": "matchPercentage"},
            {"data": "authorized"}
        ],
        'order': [[0, 'asc']],
        'columnDefs': [
            {"orderable": false,
                "targets": [2],
                "visible": false,
                "searchable": false
            },
            {

                "targets": [4, 5, 6, 7], 
                "className": "text-center"

            },
         
            {'render': function (data, type, row) {
                    return getEventTime(data);
                }, 'targets': 1},
            {'render': function (data, type, row) {
                    return getUserName(data, row['authorized']);
                }, 'targets': 3},
            {'render': function (data, type, row) {
                    return showCamImage(data, row['authorized']);
                }, 'targets': 4},
            {'render': function (data, type, row) {
                    return showEnrolledImage(data, row['authorized']);
                }, 'targets': 5},
            {'render': function (data, type, row) {
                    return getMatchPercentage(data);
                }, 'targets': 7},
            {'render': function (data, type, row) {
                    return showAuthorized(data);
                }, 'targets': 8}
        ]
    });


    /* setInterval(function () {
     eventsTable.ajax.reload();
     }, 50000);
     */



}




function getEventTime(approvalTime) {
    var timeStr = "";
    if (approvalTime !== "")
        var timeStr = moment.unix(approvalTime).format("DD MMM YYYY, hh:mm:ss A");

    return timeStr;
}

function getMatchPercentage(score) {

    if (score !== 0)
        var matchStr = score.toFixed(2);
    else
        matchStr = 0;

    return matchStr;
}



function getUserName(name, authorized) {
    if (authorized === "Yes") {
        return name;
    } else {
        return "?";
    }
}

function showCamImage(imageStr, authorized) {
    var imageUrl = "";
    var imgElement = "";
    if (authorized === "Yes") {
        imgElement = '<div class= "img-circle green-border">'
        imageUrl = '<img class="img-circle" src="data:image/jpeg;base64,';
        imgElement = imgElement + imageUrl + imageStr + '"></div>';
    } else {
        imgElement = '<div class= "img-circle red-border">'
        imageUrl = '<img class="img-circle" src="data:image/jpeg;base64,';
        imgElement = imgElement + imageUrl + imageStr + '"></div>';
    }

    return imgElement;


}



function showEnrolledImage(imageStr, authorized) {
    var imageUrl = "";
    var imgElement = "";
    if (authorized === "Yes") {
        imgElement = '<div class= "img-circle green-border">'
        imageUrl = '<img class="img-circle" src="data:image/jpeg;base64,';
        imgElement = imgElement + imageUrl + imageStr + '"></div>';

    }

    return imgElement;

}


function showAuthorized(authorized) {

    if (authorized === "Yes") {
        //return '<img src = "css/images/yes.png">';
        return '<span class="yes">Yes</span>';
    } else {
        return '<span class="no">No</span>';
        // return '<img src = "css/images/no.png">';
    }
}